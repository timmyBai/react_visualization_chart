export const add = (a: number, b: number): number => {
    return a + b;
};

test('test add function', () => {
    expect(add(5, 2)).toBe(7);
});
