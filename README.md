# react visualization chart

<div>
  <a href="https://react.dev/">
    <img src="https://img.shields.io/badge/react-18.2.0-blue">
  </a>

  <a href="https://react.dev/">
    <img src="https://img.shields.io/badge/react dom-18.2.0-blue">
  </a>

  <a href="https://redux-toolkit.js.org/">
    <img src="https://img.shields.io/badge/@reduxjs/toolkit-1.9.7-blue">
  </a>
  
  <a href="https://www.typescriptlang.org/">
    <img src="https://img.shields.io/badge/typescript-5.3.2-blue">
  </a>

  <a href="https://d3js.org/">
    <img src="https://img.shields.io/badge/d3.js-7.8.5-blue">
  </a>
</div>

</div>

## 簡介

react visualization chart 是用於開發資料視覺化的專案，它基於 react、typescript、d3.js 實建，它使用最新前端技術，提供完整的架構建置，可以幫助您建構各種資料視覺化，相信不管遇到什麼需求，這個專案可以幫助您，建置完美視覺化圖表。

## 功能

```tex
- 組織圖
- 長條圖
- 直方圖
- 折線圖
- 圓形長條圖
- 箱型圖
- 圓環圖
- 圓餅圖
- 面積折線圖
- 堆疊面積折現圖
- 流程圖
```

## 開發

```git
# 克隆項目
git clone https://gitlab.com/timmyBai/react_visualization_chart.git

# 進入目錄
cd react_visualization_chart

# 安裝依賴
npm install
or
yarn install

# 啟用專案
npm run start
or
yarn start
```

## 打包

```bash
# production
npm run build
或
yarn build
```

## 打包 Docker

```docker
# production
docker-compose -f docker-compose.yml up --build -d
```

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
