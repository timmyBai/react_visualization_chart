const express = require('express');
const path = require('path');

const app = express();
const port = 5200;

const reactBuild = path.join(__dirname, '.', 'build');
app.use(express.static(reactBuild));

app.use('*', (req, res) => {
    res.sendFile(path.join(reactBuild, 'index.html'));
});

app.listen(port);
console.log('Server started at http://localhost:' + port);