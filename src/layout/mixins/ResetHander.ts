import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// store
import { RootState } from '@/store';

const { body } = document;
const WIDTH = 992;

const ResetHander = (): void => {
    const location = useLocation();
    const dispatch = useAppDispatch();
    const storeGetters = useAppSelector((state: RootState) => {
        return {
            sidebar: state.app.sidebar,
            device: state.app.device
        };
    });

    useEffect(() => {
        if ($_isMobile()) {
            dispatch({ type: 'app/toggleDevice', payload: 'mobile' });
            dispatch({ type: 'app/closeSideBar', payload: { withoutAnimation: true }});
        }
    }, []);

    useEffect(() => {
        window.addEventListener('resize', $_resizeHander);

        return () => {
            window.removeEventListener('resize', $_resizeHander);
        };
    }, []);

    useEffect(() => {
        if (storeGetters.device === 'mobile' && storeGetters.sidebar.opened) {
            dispatch({ type: 'app/closeSideBar', payload: { withoutAnimation: false }});
        }
    }, [location]);

    const $_isMobile = (): boolean => {
        const rect = body.getBoundingClientRect();
        return rect.width - 1 < WIDTH;
    };

    const $_resizeHander = (): void => {
        const isMobile = $_isMobile();
        dispatch({ type: 'app/toggleDevice', payload: isMobile ? 'mobile' : 'desktop'});

        if (isMobile) {
            dispatch({ type: 'app/closeSideBar', payload: { withoutAnimation: true }});
        }
    };
};

export default ResetHander;
