import React, { ReactNode } from 'react';

// css
import './index.sass';

// hook
import { useAppDispatch } from '@/hook/store';

// components
import Hamburger from '@/components/Hamburger';

const Navbar: React.FC = (): ReactNode => {
    const dispatch = useAppDispatch();

    const handleCollapse = (): void => {
        dispatch({ type: 'app/toggleSideBar' });
    };

    return (
        <div className='navbar'>
            <Hamburger collapse={handleCollapse}></Hamburger>
        </div>
    );
};

export default Navbar;