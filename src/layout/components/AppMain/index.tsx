import React, { ReactNode } from 'react';
import { Outlet } from 'react-router-dom';

// css
import './index.sass';

const AppMain: React.FC = (): ReactNode => {
    return (
        <section className='appMain'>
            <Outlet></Outlet>
        </section>
    );
};

export default AppMain;