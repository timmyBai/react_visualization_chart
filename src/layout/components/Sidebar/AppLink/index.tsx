import React, { ReactNode } from 'react';
import { NavLink } from 'react-router-dom';

// css
import './index.sass';

type AppLinkProps = {
    to: string;
    title: string;
    isNest: boolean;
};

const AppLink: React.FC<AppLinkProps> = ({ to, title, isNest }): ReactNode => {
    return (
        <NavLink
            className={isNest ? 'nestLink' : 'link'}
            to={to}
        >
            {title}
        </NavLink>
    );
};

export default AppLink;
