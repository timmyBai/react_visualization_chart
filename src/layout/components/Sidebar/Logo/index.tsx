import React, { ReactNode } from 'react';
import { NavLink } from 'react-router-dom';

// css
import './index.sass';

const Logo: React.FC = (): ReactNode => {
    return (
        <div className='logo'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <NavLink to='/'>
                            <h2 className='title'>react visualization chart</h2>
                        </NavLink>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Logo;