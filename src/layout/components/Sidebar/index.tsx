import React, { ReactNode } from 'react';

// css
import './index.sass';

// hook
import { useAppSelector } from '@/hook/store';

// components
import Menu from '@/components/Menu';
import SidebarItem from './SidebarItem';
import Logo from './Logo';

const Sidebar: React.FC = (): ReactNode => {
    const storeGetters = useAppSelector((state) => {
        return {
            opened: !state.app.sidebar.opened ? 'close' : 'opened',
            device: state.app.device,
            routes: state.permission.routes
        };
    });

    return (
        <div className='sidebar'>
            <div className='sidebar_container'>
                <Logo></Logo>
                <Menu>
                    {
                        storeGetters.routes.map((route: any) => {
                            return (
                                <SidebarItem
                                    item={route}
                                    basePath={route.path}
                                    isNest={false}
                                    key={route.path}
                                ></SidebarItem>
                            );
                        })
                    }
                </Menu>
            </div>
        </div>
    );
};

export default Sidebar;