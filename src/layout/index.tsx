import React, { ReactNode } from 'react';

// css
import './index.sass';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// components
import AppMain from './components/AppMain';
import Sidebar from './components/Sidebar';
import Navbar from './components/Navbar';
import ResetHander from './mixins/ResetHander';

const Layout: React.FC = (): ReactNode => {
    ResetHander();

    const dispatch = useAppDispatch();
    const storeGetters = useAppSelector((state) => {
        return {
            device: state.app.device === 'mobile' ? 'mobile' : '',
            opened: state.app.sidebar.opened ? 'opened' : '',
            close: !state.app.sidebar.opened ? 'close' : '',
            withoutAnimation: state.app.sidebar?.withoutAnimation ? 'withoutAnimation' : ''
        };
    });

    const handleClickOutside = (): void => {
        dispatch({
            type: 'app/closeSideBar',
            payload: { withoutAnimation: false }
        });
    };

    return (
        <div className={`appWapper ${storeGetters.device} ${storeGetters.opened} ${storeGetters.close} ${storeGetters.withoutAnimation}`}>
            {storeGetters.device === 'mobile' && storeGetters.opened && <div className='draw' onClick={handleClickOutside}></div>}

            <Sidebar></Sidebar>

            <div className='main_container'>
                <Navbar></Navbar>
                <AppMain></AppMain>
            </div>
        </div>
    );
};

export default Layout;