import React, { Fragment, ReactNode, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const Permission: React.FC = (): ReactNode => {
    const navigate = useNavigate();
    const location = useLocation();
    
    const [token] = useState(true);

    useEffect(() => {
        main();
    }, [location.pathname, token]);

    // main
    const main = (): void => {
        const whiteList = ['/404'];

        if (token) {
            navigate(location.pathname);
        }
        else {
            if (whiteList.indexOf(location.pathname) !== -1) {
                navigate(location.pathname);
            }
            else {
                navigate('/404');
            }
        }
    };

    return <Fragment></Fragment>;
};

export default Permission;