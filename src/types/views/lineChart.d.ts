type LineChartBasicCsvDataType = {
    date: Date;
    value: string;
};

type LineChartColorGradientCsvDataType = {
    date: Date;
    value: string;
};

type LineChartInputDataTransferType = {
    ser1: number;
    ser2: number;
}

type LineChartZoom = {
    date: Date;
    value: number;
};

type LinearGradient = {
    offset: string;
    color: string;
};