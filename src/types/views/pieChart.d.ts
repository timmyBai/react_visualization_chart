type PieChartBasicDataType = {
    a: number;
    b: number;
    c: number;
    d: number;
    e: number;
}

type PieChartAnnotationDataType = {
    a: number;
    b: number;
    c: number;
    d: number;
    e: number;
}

type PieChartInputDataSelectorData1Type = {
    a: number;
    b: number;
    c: number;
    d: number;
    e: number;
}

type PieChartInputDataSelectorData2Type = {
    a: number;
    b: number;
    c: number;
    d: number;
    e: number;
    f: number;
}