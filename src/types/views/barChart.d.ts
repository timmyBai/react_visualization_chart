type BarChartChangeInputDataType = {
    group: string;
    value: number;
};

type BarChartChangeInputDataUpgradeType = {
    group: string;
    value: number;
};

type BarChartGroupedKeyDataType = {
    key: string;
    value: string;
};
