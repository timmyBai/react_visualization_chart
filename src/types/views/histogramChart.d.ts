type HistogramChartBasicCsvDataType = {
    price: number
}

type HistogramChartToolTipCsvDataType = {
    price: number
}

type HistogramChartColoredCsvType = {
    price: number
}