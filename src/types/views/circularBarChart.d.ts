type CircularBarChartBasicCsvDataType = {
    Country: string;
    Value: number;
};

type CircularBarChartLabelCsvDataType = {
    Country: string;
    Value: number;
}

type CircularBarChartDoubleCsvDataType = {
    Country: string;
    Value: number;
}

type CircularBarChartStackedCsvDataType = {
    'State': string;
    'Under 5 Years': number;
    '5 to 13 Years': number;
    '14 to 17 Years': number;
    '18 to 24 Years': number;
    '25 to 44 Years': number;
    '45 to 64 Years': number;
    '65 Years and Over': number;
    total: number;
}
