type AreaChartBasicCsvDataType = {
    date: Date;
    value: number;
};

type AreaChartPointsCsvDataType = {
    date: Date;
    value: number;
}

type AreaChartZoomCsvDataType = {
    date: Date;
    value: number;
}

type AreaChartAdvancedCsvType = {
    date: Date;
    close: number;
}

type AreaChartOverlappingAreasCsvType = {
    date: Date;
    PVkW: number;
    TBLkW: number;
}

type AreaChartSmallMultipleCsvType = {
    year: number,
    sex: string,
    name: string,
    n: number,
    prop: number
}

type AreaChartZoomWithFocusCsvType = {
    date: Date;
    price: number;
}