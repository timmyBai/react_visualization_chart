type DonutChartBasicType = {
    a: number;
    b: number;
    c: number;
    d: number;
    e: number;
}

type DonutChartLabelDataType = {
    a: number;
    b: number;
    c: number;
    d: number;
    e: number;
    f: number;
    g: number;
    h: number;
};

type DonutChartCleanLayoutRandomDataType = {
    label: string;
    value: number;
};