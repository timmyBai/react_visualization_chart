type AppStateType = {
    isLoading: boolean;
    sidebar: AppSidebarType,
    device: 'desktop' | 'mobile'
};

type AppSidebarType = {
    opened: boolean;
    withoutAnimation: boolean;
}