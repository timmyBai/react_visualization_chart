type MarginType = {
    top: number;
    right: number;
    bottom: number;
    left: number;
};