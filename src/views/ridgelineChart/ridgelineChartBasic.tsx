import React, { ReactNode } from 'react';

// css
import './ridgelineChartBasic.sass';

const RidgelineChartBasic: React.FC = (): ReactNode => {
    return (
        <div className='ridgelineChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='ridgelineChartBasicSvg'>
                            基礎山脊圖
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default RidgelineChartBasic;