import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './boxPlotChartBasic.sass';

const BoxPlotChartBasic: React.FC = (): ReactNode => {
    const boxPlotChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 40, left: 55 };

    const handleReSize = (): void => {
        if (boxPlotChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(boxPlotChartBasicRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBoxPlotChartBasic = useCallback(() => {
        d3.select('.boxPlotChartBasicSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.boxPlotChartBasicSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        const data: number[] = [12, 19, 11, 13, 12, 22, 13, 4, 15, 16, 18, 19, 20, 12, 11, 9];

        const data_sort = data.sort(d3.ascending);
        const q1 = d3.quantile(data_sort, 0.25) as number;
        const median = d3.quantile(data_sort, 0.5);
        const q3 = d3.quantile(data_sort, 0.75) as number;
        const interQuantileRange = q3 - q1;
        const min = q1 - 1.5 * interQuantileRange;
        const max = q1 + 1.5 * interQuantileRange;

        const y = d3.scaleLinear()
            .domain([0, d3.max(data_sort, (d) => +d)] as [number, number])
            .range([height, 0]);

        svg.append('g')
            .call(d3.axisLeft(y));

        svg.append('line')
            .attr('x1', width / 2)
            .attr('x2', width / 2)
            .attr('y1', y(min))
            .attr('y2', y(max))
            .attr('stroke', 'black');

        svg.append('rect')
            .attr('x', width / 2 - width / 4)
            .attr('y', y(q3))
            .attr('height', (y(q1) - y(q3)))
            .attr('width', width / 2)
            .attr('stroke', 'black')
            .style('fill', colors.color_purple);

        svg.selectAll('toto')
            .data([min, median, max])
            .enter()
            .append('line')
            .attr('x1', width / 2 - width / 4)
            .attr('x2', width / 2 + width / 4)
            .attr('y1', (d: any) => {
                return y(d) as number;
            })
            .attr('y2', (d: any) => y(d))
            .attr('stroke', 'black');
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleBoxPlotChartBasic();
        window.addEventListener('resize', () => {
            setTimeout(() => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', () => {
                setTimeout(() => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleBoxPlotChartBasic]);

    return (
        <div className='boxPlotChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='boxPlotChartBasicSvg' ref={boxPlotChartBasicRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BoxPlotChartBasic;