import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './boxPlotChartGroup.sass';

const BoxPlotChartGroup: React.FC = (): ReactNode => {
    const boxPlotChartGroupRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 40, left: 55 };

    const handleReSize = (): void => {
        if (boxPlotChartGroupRef.current?.clientWidth !== undefined) {
            setWidth(boxPlotChartGroupRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBoxPlotChartGroup = useCallback(() => {
        d3.select('.boxPlotChartGroupSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.boxPlotChartGroupSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/iris.csv').then((data): void => {
            const groups = d3.group(data, d => d.Species);
            const sumstat = d3.rollup(groups, d => d[0][0], (d) => {
                const q1 = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.25) as number;
                const median = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.5) as number;
                const q3 = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.75) as number;
                const interQuantileRange = q3 - q1;
                const min = q1 - 1.5 * interQuantileRange;
                const max = q3 + 1.5 * interQuantileRange;

                return {
                    q1,
                    median,
                    q3,
                    interQuantileRange,
                    min,
                    max
                };
            });

            const xDomain = d3.map(sumstat, (d) => d[1]);

            const x = d3.scaleBand()
                .domain(xDomain)
                .range([0, width])
                .padding(0.7);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const scaleNumberList = d3.map(sumstat, (d) => {
                return [d[0].min, d[0].max];
            }).join(',').split(',');

            const y = d3.scaleLinear()
                .domain(d3.extent(scaleNumberList, d => +d) as [number, number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.append('g')
                .selectAll('line')
                .data(sumstat)
                .enter()
                .append('line')
                .attr('x1', (d) => {
                    return x(d[1]) as number + x.bandwidth() / 2;
                })
                .attr('x2',(d) => {
                    return x(d[1]) as number + x.bandwidth() / 2;
                })
                .attr('y1', (d) => {
                    return y(d[0].min);
                })
                .attr('y2', (d) => {
                    return y(d[0].max);
                })
                .attr('stroke', 'black');

            svg.selectAll('boxs')
                .data(sumstat)
                .enter()
                .append('rect')
                .attr('fill', colors.color_blue)
                .attr('x', (d) => {
                    return x(d[1]) as number;
                })
                .attr('y', (d: any) => y(d[0].q3) as number)
                .attr('height', (d: any) => y(d[0].q1) - y(d[0].q3))
                .attr('width', x.bandwidth());

            svg.selectAll('toto')
                .data(sumstat)
                .enter()
                .append('line')
                .attr('x1', d => x(d[1]) as number)
                .attr('x2', d => x(d[1]) as number + x.bandwidth())
                .attr('y1', d => y(d[0].median) as number)
                .attr('y2', d => y(d[0].median) as number)
                .attr('stroke', 'black')
                .style('width', 80);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleBoxPlotChartGroup();
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [handleBoxPlotChartGroup]);

    return (
        <div className='boxPlotChartGroup'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='boxPlotChartGroupSvg' ref={boxPlotChartGroupRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BoxPlotChartGroup;