import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './boxPlotChartHorizontal.sass';

const BoxPlotChartHorizontal: React.FC = (): ReactNode => {
    const boxPlotChartHorizontalRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 40, left: 55 };

    const handleReSize = (): void => {
        if (boxPlotChartHorizontalRef.current?.clientWidth !== undefined) {
            setWidth(boxPlotChartHorizontalRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBoxPlotChartHorizontal = useCallback(() => {
        d3.select('.boxPlotChartHorizontalSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.boxPlotChartHorizontalSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/iris.csv').then((data): void => {
            const groups = d3.group(data, d => d.Species);
            const sumstat = d3.rollup(groups, d => d[0][0], (d) => {
                const q1 = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.25) as number;
                const median = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.5) as number;
                const q3 = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.75) as number;
                const interQuantileRange = q3 - q1;
                const min = q1 - 1.5 * interQuantileRange;
                const max = q3 + 1.5 * interQuantileRange;

                return {
                    q1,
                    median,
                    q3,
                    interQuantileRange,
                    min,
                    max
                };
            });

            const scaleNumberList = d3.map(sumstat, (d) => {
                return [d[0].min, d[0].max];
            }).join(',').split(',');

            const x = d3.scaleLinear()
                .domain(d3.extent(scaleNumberList, d => +d) as [number, number])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const yDomain = d3.map(sumstat, (d) => d[1]);

            const y = d3.scaleBand()
                .domain(yDomain)
                .range([height, 0])
                .padding(0.5);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.append('g')
                .selectAll('line')
                .data(sumstat)
                .enter()
                .append('line')
                .attr('x1', (d: any) => {
                    return x(d[0].min) as number;
                })
                .attr('x2', (d: any) => {
                    return x(d[0].max) as number;
                })
                .attr('y1', (d: any) => {
                    return y(d[1]) as number + y.bandwidth() / 2;
                })
                .attr('y2', (d: any) => {
                    return y(d[1]) as number + y.bandwidth() / 2;
                })
                .attr('stroke', 'black');

            svg.selectAll('boxs')
                .data(sumstat)
                .enter()
                .append('rect')
                .attr('fill', colors.color_blue)
                .attr('height', y.bandwidth())
                .attr('width', (d: any) => x(d[0].q3) - x(d[0].q1))
                .attr('x', (d: any) => x(d[0].q1) as number)
                .attr('y', (d) => {
                    return y(d[1]) as number;
                });

            svg.selectAll('toto')
                .data(sumstat)
                .enter()
                .append('line')
                .attr('x1', d => x(d[0].median) as number)
                .attr('x2', d => x(d[0].median) as number)
                .attr('y1', d => y(d[1]) as number)
                .attr('y2', d => y(d[1]) as number + y.bandwidth())
                .attr('stroke', 'black')
                .style('width', 80);

            const color = d3.scaleSequential()
                .interpolator(d3.interpolateInferno)
                .domain([3, 6]);

            const circle = svg.append('g')
                .selectAll('circle')
                .data(data)
                .enter()
                .append('circle')
                .attr('fill', d => color(+d.Sepal_Length))
                .attr('stroke', colors.color_black)
                .attr('cy', (d) => {
                    const xRandom = Math.random();

                    if (xRandom < 0.5) {
                        return y(d.Species) as number + y.bandwidth() / 2 - Math.random() * y.bandwidth() / 2;
                    }

                    return y(d.Species) as number + y.bandwidth() / 2 + Math.random() * y.bandwidth() / 2;
                })
                .attr('cx', (d) => x(+d.Sepal_Length) as number)
                .attr('r', 5);
            
            const mouseOver = (): void => {
                d3.selectAll('.tag')
                    .style('display', 'inline-block');
            };

            const mouseMove = (e: MouseEvent, d: any): void => {
                const tagElement = d3.select('.tag').node() as HTMLDivElement;

                d3.select('.tag')
                    .style('left', () => {
                        if (e.offsetX < width / 2) {
                            return `${e.offsetX + 3}px`;
                        }

                        return `${e.offsetX - tagElement.clientWidth - 3}px`;
                    })
                    .style('top', `${e.offsetY + 3}px`)
                    .style('display', 'inline-block')
                    .html(`
                        <span>type: ${d.Species}</span>
                        </br>
                        <span>Sepal_Length: ${d.Sepal_Length}</span>
                    `);
            };

            const mouseLeave = (): void => {
                d3.selectAll('.tag')
                    .style('left', '0px')
                    .style('top', '0px')
                    .style('display', 'none')
                    .html('');
            };

            circle.on('mouseover', mouseOver);
            circle.on('mousemove', mouseMove);
            circle.on('mouseleave', mouseLeave);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleBoxPlotChartHorizontal();
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [handleBoxPlotChartHorizontal]);

    return (
        <div className='boxPlotChartHorizontal'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            <div className='tag'></div>

                            <div className='boxPlotChartHorizontalSvg' ref={boxPlotChartHorizontalRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BoxPlotChartHorizontal;
