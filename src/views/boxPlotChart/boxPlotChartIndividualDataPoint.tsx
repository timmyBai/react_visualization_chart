import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './boxPlotChartIndividualDataPoint.sass';

const BoxPlotChartIndividualDataPoint: React.FC = (): ReactNode => {
    const boxPlotChartIndividualDataPointRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 40, left: 55 };

    const handleReSize = (): void => {
        if (boxPlotChartIndividualDataPointRef.current?.clientWidth !== undefined) {
            setWidth(boxPlotChartIndividualDataPointRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBoxPlotChartIndividualDataPoint = useCallback(() => {
        d3.select('.boxPlotChartIndividualDataPointSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.boxPlotChartIndividualDataPointSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/iris.csv').then((data) => {
            const groups = d3.group(data, d => d.Species);
            const sumstat = d3.rollup(groups, d => d[0][0], (d) => {
                const q1 = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.25) as number;
                const median = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.5) as number;
                const q3 = d3.quantile(d[1].map(g => +g.Sepal_Length).sort(d3.ascending), 0.75) as number;
                const interQuantileRange = q3 - q1;
                const min = q1 - 1.5 * interQuantileRange;
                const max = q3 + 1.5 * interQuantileRange;

                return {
                    q1,
                    median,
                    q3,
                    interQuantileRange,
                    min,
                    max
                };
            });

            const xDoamin = d3.map(sumstat, (d) => d[1]);

            const x = d3.scaleBand()
                .domain(xDoamin)
                .range([0, width])
                .padding(0.7);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const scaleNumberList = d3.map(sumstat, (d) => {
                return [d[0].min, d[0].max];
            }).join(',').split(',');

            const y = d3.scaleLinear()
                .domain(d3.extent(scaleNumberList, d => +d) as [number, number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.append('g')
                .selectAll('line')
                .data(sumstat)
                .enter()
                .append('line')
                .attr('x1', (d) => {
                    return x(d[1]) as number + x.bandwidth() / 2;
                })
                .attr('x2', (d) => {
                    return x(d[1]) as number + x.bandwidth() / 2;
                })
                .attr('y1', (d) => {
                    return y(d[0].min);
                })
                .attr('y2', (d) => {
                    return y(d[0].max);
                })
                .attr('stroke', 'black');

            svg.selectAll('boxs')
                .data(sumstat)
                .enter()
                .append('rect')
                .attr('fill', colors.color_blue)
                .attr('x', (d) => {
                    return x(d[1]) as number;
                })
                .attr('y', (d: any) => y(d[0].q3) as number)
                .attr('height', (d: any) => y(d[0].q1) - y(d[0].q3))
                .attr('width', x.bandwidth());

            svg.selectAll('toto')
                .data(sumstat)
                .enter()
                .append('line')
                .attr('x1', d => x(d[1]) as number)
                .attr('x2', d => x(d[1]) as number + x.bandwidth())
                .attr('y1', d => y(d[0].median) as number)
                .attr('y2', d => y(d[0].median) as number)
                .attr('stroke', 'black')
                .style('width', 80);

            svg.selectAll('inPoints')
                .data(data)
                .enter()
                .append('circle')
                .attr('fill', colors.color_purple)
                .attr('stroke', colors.color_black)
                .attr('cx', (d) => {
                    const xRandom = Math.random();

                    if (xRandom < 0.5) {
                        return x(d.Species) as number + x.bandwidth() / 2 - Math.random() * x.bandwidth() / 2;
                    }

                    return x(d.Species) as number + x.bandwidth() / 2 + Math.random() * x.bandwidth() / 2;
                })
                .attr('cy', (d) => y(+d.Sepal_Length) as number)
                .attr('r', 5);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleBoxPlotChartIndividualDataPoint();
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [handleBoxPlotChartIndividualDataPoint]);

    return (
        <div className='boxPlotChartIndividualDataPoint'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='boxPlotChartIndividualDataPointSvg' ref={boxPlotChartIndividualDataPointRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BoxPlotChartIndividualDataPoint;