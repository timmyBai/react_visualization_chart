import React, { ReactNode } from 'react';

// css
import './correlogramBasic.sass';

const CorrelogramBasic: React.FC = (): ReactNode => {
    return (
        <div className='correlogramBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='correlogramBasicSvg'>
                            基礎相關圖
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CorrelogramBasic;