import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './donutChartCleanLayout.sass';

const DonutChartCleanLayout: React.FC = (): ReactNode => {
    const donutChartCleanLayoutRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };
    
    const handleReSize = (): void => {
        if (donutChartCleanLayoutRef.current?.clientWidth !== undefined) {
            setWidth(donutChartCleanLayoutRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleDonutChartCleanLayout = useCallback((): void => {
        d3.select('.donutChartCleanLayoutSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.donutChartCleanLayoutSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height})`);

        const radius: number = Math.min(width, height) / 2;

        const arc: d3.Arc<any, d3.DefaultArcObject> = d3.arc()
            .innerRadius(radius * 0.4)
            .outerRadius(radius * 0.8);
        
        const outerArc: d3.Arc<any, d3.DefaultArcObject> = d3.arc()
            .innerRadius(radius * 0.9)
            .outerRadius(radius * 0.9);

        const pie: d3.Pie<any, number | {
            valueOf(): number;
        }> = d3.pie()
            .value((d: any): number => d[1].value);

        const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
            .domain([
                'Lorem ipsum',
                'dolor sit',
                'amet',
                'consectetur',
                'adipisicing',
                'elit',
                'sed',
                'do',
                'eiusmod',
                'tempor',
                'incididunt'
            ])
            .range([
                colors.color_black,
                colors.color_blue,
                colors.color_pink,
                colors.color_purple,
                colors.color_dark_purple,
                colors.color_light_pink,
                colors.color_light_blue,
                colors.color_light_black,
                colors.color_light_pink
            ]);
        
        const randomData = (): DonutChartCleanLayoutRandomDataType[] => {
            const labels = color.domain();
            return labels.map((label) => {
                return {
                    label,
                    value: Math.random()
                };
            });
        };

        const data_ready: d3.PieArcDatum<number | {
            valueOf(): number;
        }>[] = pie(Object.entries(randomData()) as any);
        
        svg.append('g')
            .selectAll('path')
            .data(data_ready)
            .enter()
            .append('path')
            .attr('fill', (d: any): string => {
                return color(d.data[1].value) as string;
            })
            .attr('d', arc as any)
            .attr('stroke', colors.color_black)
            .attr('stroke-width', 0.7);

        svg.append('g')
            .selectAll('polyline')
            .data(data_ready)
            .enter()
            .append('polyline')
            .attr('fill', 'none')
            .attr('stroke', colors.color_black)
            .attr('stroke-width', 0.1)
            .attr('points', (d: any): [number, number, number] => {
                const posA: [number, number] = arc.centroid(d);
                const posB: [number, number] = outerArc.centroid(d);
                const posC: [number, number] = outerArc.centroid(d);
                const midangle: number = d.startAngle + (d.endAngle - d.startAngle) / 2;
                posC[0] = radius * 0.95 * (midangle < Math.PI ? 1 : -1);

                return [posA, posB, posC] as any;
            });

        svg.append('g')
            .selectAll('text')
            .data(data_ready)
            .enter()
            .append('text')
            .attr('transform', (d: d3.PieArcDatum<number | {
                valueOf(): number;
            }>): string => {
                const pos: [number, number] = outerArc.centroid(d as any);
                const midangle: number = d.startAngle + (d.endAngle - d.startAngle) / 2;
                pos[0] = radius * 0.99 * (midangle < Math.PI ? 1 : -1);

                return `translate(${pos})`;
            })
            .style('text-anchor', (d): string => {
                const midangle: number = d.startAngle + (d.endAngle - d.startAngle) / 2;
                
                return (midangle < Math.PI ? 'start' : 'end');
            })
            .style('font-size', '12px')
            .text((d: any): string => {
                return d.data[1].label as string;
            });
    }, [width, height]);

    const handleRandomize = (): void => {
        handleDonutChartCleanLayout();
    };

    useEffect(() => {
        handleReSize();
        handleDonutChartCleanLayout();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleDonutChartCleanLayout]);
    
    return (
        <div className='donutChartCleanLayout'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            <button className='btn randomizeBtn' onClick={handleRandomize}>randomize</button>

                            <div className='donutChartCleanLayoutSvg' ref={donutChartCleanLayoutRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DonutChartCleanLayout;