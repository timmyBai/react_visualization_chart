import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './donutChartChangeData.sass';

const DonutChartChangeData: React.FC = (): ReactNode => {
    const donutChartUpdateDataRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const [fruitType, setFruitType] = useState<string[]>([]);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };
    
    const handleReSize = (): void => {
        if (donutChartUpdateDataRef.current?.clientWidth !== undefined) {
            setWidth(donutChartUpdateDataRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleDonutChartUpdateData = useCallback((type: string): void => {
        d3.select('.donutChartChangeDataSvg').select('svg').remove();
        
        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.donutChartChangeDataSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height})`);

        d3.tsv('https://gist.githubusercontent.com/tezzutezzu/c2653d42ffb4ecc01ffe2d6c97b2ee5e/raw/1063b1fbdf95731840eab263d3595415e6ea5a38/data.tsv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            data.forEach((item: d3.DSVRowString<string>): void => {
                if (fruitType.indexOf(item.fruit) === -1) {
                    setFruitType(fruitType.concat(item.fruit));
                }
            });
        
            const groups: d3.DSVRowString<string>[] = d3.filter(data, d => d.fruit === type);

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .range([colors.color_blue, colors.color_pink, colors.color_purple, colors.color_light_pink, colors.color_dark_purple]);

            const pie: d3.Pie<any, number | {
                valueOf(): number;
            }> = d3.pie()
                .value((d: any): number => {
                    return d[1].count;
                });

            const group_ready: d3.PieArcDatum<number | {
                valueOf(): number;
            }>[] = pie(Object.entries(groups) as any);

            const radius: number = Math.min(width, height) / 2;

            svg.append('g')
                .selectAll('path')
                .data(group_ready)
                .enter()
                .append('path')
                .attr('fill', (d: d3.PieArcDatum<number | {
                    valueOf(): number;
                }>): string => {
                    return color(d as any) as string;
                })
                .attr('d', d3.arc<any>()
                    .innerRadius(100)
                    .outerRadius(radius)
                );
        });
    }, [width, height, fruitType]);

    const handleChangeFruitTypeInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        handleDonutChartUpdateData(e.target.value);
    };

    useEffect(() => {
        handleReSize();
        handleDonutChartUpdateData('Apples');
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleDonutChartUpdateData]);

    return (
        <div className='donutChartChangeData'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            {
                                fruitType.map((item, index) => {
                                    return (
                                        <label key={index}>
                                            <input type='radio' defaultValue={item} onChange={handleChangeFruitTypeInput} name='fruitType' defaultChecked={item === 'Apples'} />
                                            <span>{item}</span>
                                        </label>
                                    );
                                })
                            }

                            <div className='donutChartChangeDataSvg' ref={donutChartUpdateDataRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DonutChartChangeData;