import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './donutChartLabel.sass';

const DonutChartLabel: React.FC = (): ReactNode => {
    const donutChartLabelRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const handleReSize = (): void => {
        if (donutChartLabelRef.current?.clientWidth !== undefined) {
            setWidth(donutChartLabelRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleDonutChartLabel = useCallback((): void => {
        d3.select('.donutChartLabelSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.donutChartLabelSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height})`);

        const data: DonutChartLabelDataType = { a: 9, b: 20, c: 30, d: 8, e: 12, f: 3, g: 7, h: 14 };

        const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
            .domain(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'])
            .range(d3.schemeDark2);

        const pie: d3.Pie<any, number | {
            valueOf(): number;
        }> = d3.pie()
            .value((d: any) => d[1]);

        const data_ready: d3.PieArcDatum<number | {
            valueOf(): number;
        }>[] = pie(Object.entries(data) as any);

        const radius: number = Math.min(width, height) / 2;

        const arc: d3.Arc<any, d3.DefaultArcObject> = d3.arc()
            .innerRadius(radius * 0.5)
            .outerRadius(radius * 0.8);

        const outerArc: d3.Arc<any, d3.DefaultArcObject> = d3.arc()
            .innerRadius(radius * 0.9)
            .outerRadius(radius * 0.9);

        svg.selectAll('allSlices')
            .data(data_ready)
            .join('path')
            .attr('d', arc as any)
            .attr('fill', (d: any): string => color(d.data[1]) as string)
            .attr('stroke', 'white')
            .style('stroke-width', '2px')
            .style('opacity', 0.7);

        svg.selectAll('allPolylines')
            .data(data_ready)
            .join('polyline')
            .attr('stroke', 'black')
            .style('fill', 'none')
            .attr('stroke-width', 1)
            .attr('points', (d: any): [number, number, number] => {
                const posA: [number, number] = arc.centroid(d);
                const posB: [number, number] = outerArc.centroid(d);
                const posC: [number, number] = outerArc.centroid(d);
                const midangle: number = d.startAngle + (d.endAngle - d.startAngle) / 2;
                posC[0] = radius * 0.95 * (midangle < Math.PI ? 1 : -1);

                return [posA, posB, posC] as any;
            });

        svg.selectAll('allLabels')
            .data(data_ready)
            .join('text')
            .text((d: any): string => {
                return d.data[0] as string;
            })
            .attr('transform', (d: any): string => {
                const pos = outerArc.centroid(d as any);
                const midangle = d.startAngle + (d.endAngle - d.startAngle) / 2;
                pos[0] = radius * 0.99 * (midangle < Math.PI ? 1 : -1);

                return `translate(${pos})` as any;
            })
            .style('text-anchor', (d: any): string => {
                const midangle = d.startAngle + (d.endAngle - d.startAngle) / 2;

                return (midangle < Math.PI ? 'start' : 'end');
            });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleDonutChartLabel();
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [handleDonutChartLabel]);

    return (
        <div className='donutChartLabel'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='donutChartLabelSvg' ref={donutChartLabelRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DonutChartLabel;