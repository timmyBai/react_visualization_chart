import React, { useEffect, useState, useRef, useCallback, ReactNode } from 'react';
import * as d3 from 'd3';

import colors from '@/styles/colors.sass';
import './densityChartMultipleGroup.sass';

const DensityMultipleGroup: React.FC = (): ReactNode => {
    const densityMultipleGroupRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };

    const handleResize = (): void => {
        if (densityMultipleGroupRef.current?.clientWidth !== undefined) {
            setWidth(densityMultipleGroupRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const densityMultipleGroupChart = useCallback((): void => {
        d3.select('svg').remove();

        const svg = d3.select('.densityMultipleGroupSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_doubleHist.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([-10, 15])
                .range([0, width]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .call(d3.axisBottom(x))
                .attr('stroke-width', 0.6);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .range([height, 0])
                .domain([0, 0.12]);

            svg.append('g')
                .call(d3.axisLeft(y))
                .attr('stroke-width', 0.6);

            const kde: any = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(60));
            const densityA: any = kde(
                data.filter((d: d3.DSVRowString<string>) => d.type === 'variable 1')
                    .map((d: d3.DSVRowString<string>) => d.value)
            );
            const densityB: any = kde(
                data.filter((d: d3.DSVRowString<string>) => d.type === 'variable 2')
                    .map((d: d3.DSVRowString<string>) => d.value)
            );

            // 生產密度
            svg.append('path')
                .attr('class', 'mypath')
                .datum(densityA)
                .attr('fill', colors.color_blue)
                .attr('opacity', '.6')
                .attr('stroke', colors.color_black)
                .attr('stroke-width', '0.6')
                .attr('stroke-linejoin', 'round')
                .attr('d', d3.line()
                    .curve(d3.curveBasis)
                    .x((d: [number, number]) => x(d[0]))
                    .y((d: [number, number]) => y(d[1]))
                );

            // 生產密度
            svg.append('path')
                .attr('class', 'mypath')
                .datum(densityB)
                .attr('fill', colors.color_pink)
                .attr('opacity', '.6')
                .attr('stroke', '#000')
                .attr('stroke-width', '0.6')
                .attr('stroke-linejoin', 'round')
                .attr('d', d3.line()
                    .curve(d3.curveBasis)
                    .x((d: [number, number]) => x(d[0]))
                    .y((d: [number, number]) => y(d[1]))
                );

            svg.append('circle')
                .attr('cx', densityMultipleGroupRef.current?.clientWidth !== undefined ? densityMultipleGroupRef.current.clientWidth - 210 : 0)
                .attr('cy', 30)
                .attr('r', 6)
                .attr('fill', colors.color_blue);

            svg.append('text')
                .attr('x', densityMultipleGroupRef.current?.clientWidth !== undefined ? densityMultipleGroupRef.current.clientWidth - 180 : 0)
                .attr('y', 30)
                .text('variable 1')
                .style('font-size', '15px')
                .style('alignment-baseline', 'middle');

            svg.append('circle')
                .attr('cx', densityMultipleGroupRef.current?.clientWidth !== undefined ? densityMultipleGroupRef.current.clientWidth - 210 : 0)
                .attr('cy', 60)
                .attr('r', 6)
                .attr('fill', colors.color_pink);

            svg.append('text')
                .attr('x', densityMultipleGroupRef.current?.clientWidth !== undefined ? densityMultipleGroupRef.current.clientWidth - 180 : 0)
                .attr('y', 60)
                .text('variable 2')
                .style('font-size', '15px')
                .style('alignment-baseline', 'middle');
        });
    }, [width, height]);

    const kernelDensityEstimator = (kernel: (v: number) => number, X: any): any => {
        return (V: any) => {
            return X.map((x: any) => {
                return [x, d3.mean(V, (v: number) => kernel(x - v))];
            });
        };
    };

    const kernelEpanechnikov = (k: number): any => {
        return (v: number): number => {
            return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
        };
    };

    useEffect(() => {
        handleResize();
        densityMultipleGroupChart();

        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleResize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleResize();
                }, 100);
            });
        };
    }, [densityMultipleGroupChart]);

    return (
        <div className='densityMultipleGroup'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='rounded-md'>
                            <div className='densityMultipleGroupSvg' ref={densityMultipleGroupRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DensityMultipleGroup;