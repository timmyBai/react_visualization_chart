import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './densityHistogramChart.sass';
import colors from '@/styles/colors.sass';

const DensityHistogramChart: React.FC = (): ReactNode => {
    const densityHistogramChartRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };

    const handleResize = (): void => {
        if (densityHistogramChartRef.current?.clientWidth !== undefined) {
            setWidth(densityHistogramChartRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleDensityHistogramChart = useCallback((): void => {
        d3.select('.densityChartHistogramSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.densityChartHistogramSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        d3.json<number[]>('https://gist.githubusercontent.com/mbostock/4341954/raw/cac8843f76621446a1f61dbd7124d62783d274e6/faithful.json').then((data: number[] | undefined): void | Promise<void> => {
            const dataArr: number[] = data || [];

            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(dataArr) as number + 10])
                .range([0, width]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .call(d3.axisBottom(x));

            const histogram: d3.HistogramGeneratorNumber<any, number> = d3.bin<any, number>()
                .domain(x.domain() as [number, number]).thresholds(40);

            const bins: d3.Bin<any, number>[] = histogram(dataArr);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([
                    0,
                    d3.extent(bins, (d: number[]) => d.length / dataArr.length)[1] as number
                ])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y).ticks(null, '%'));

            const kde: any = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(40));
            const density: any = kde(dataArr.map((d: number) => d));

            svg.selectAll('rect')
                .data(bins)
                .join('rect')
                .attr('type', 'monotone')
                .attr('x', 1)
                .attr('transform', (d: d3.Bin<any, number>) => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;

                    return `translate(${x(x1)}, ${height}) rotate(180)`;
                })
                .attr('height', 0)
                .attr('fill', colors.color_blue)
                .transition()
                .duration(2000)
                .delay((d: d3.Bin<any, number>, i: number) => {
                    return 30 * i;
                })
                .ease(d3.easeElastic)
                .attr('width', (d: d3.Bin<any, number>) => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;
                    const x0 = d.x0 !== undefined ? d.x0 : 0;

                    if (x(x1) - x(x0) === 0) {
                        return x(x1) - x(x0);
                    }
                    else {
                        return `${x(x1) - x(x0) - 1}`;
                    }
                })
                .attr('height', (d: d3.Bin<any, number>) => {
                    return `${height - y(d.length / dataArr.length)}`;
                });

            const path: d3.Selection<SVGPathElement, any, HTMLElement, any> = svg.append('path')
                .datum(density)
                .attr('fill', colors.color_pink)
                .attr('opacity', 0.8)
                .attr('stroke', colors.color_black)
                .attr('stroke-width', 0.6)
                .attr('stroke-linejoin', 'round');

            const totalLength: number = path.node()?.getTotalLength() as number;

            path.attr('stroke-dasharray', totalLength)
                .attr('stroke-dashoffset', totalLength)
                .transition()
                .duration(2000)
                .ease(d3.easeLinear)
                .attr('stroke-dashoffset', 0)
                .attr('d', d3.line()
                    .curve(d3.curveBasis)
                    .x((d: [number, number]) => {
                        return x(d[0]);
                    })
                    .y((d: [number, number]) => {
                        return y(d[1]);
                    })
                );
        });

        const kernelDensityEstimator = (kernel: (v: number) => number, X: any): any => {
            return (V: any) => {
                return X.map((x: any) => {
                    return [x, d3.mean(V, (v: number) => {
                        return kernel(x - v);
                    })];
                });
            };
        };

        const kernelEpanechnikov = (k: number): any => {
            return (v: number): number => {
                return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
            };
        };
    }, [width, height]);

    useEffect(() => {
        handleResize();
        handleDensityHistogramChart();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleResize();
            }, 100);
        });

        return (
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleResize();
                }, 100);
            })
        );
    }, [handleDensityHistogramChart]);

    return (
        <div className='densityChartHistogram'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='rounded-md'>
                            <div className='densityChartHistogramSvg' ref={densityHistogramChartRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DensityHistogramChart;