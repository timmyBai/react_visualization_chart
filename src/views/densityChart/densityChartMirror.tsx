import React, { useEffect, useState, useRef, ReactNode } from 'react';
import * as d3 from 'd3';

// styles
import colors from '@/styles/colors.sass';
import './densityChartMirror.sass';

const DensityChartMirror: React.FC = (): ReactNode => {
    const densityChartMirrorSvgRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };

    const handleResize = (): void => {
        if (densityChartMirrorSvgRef.current?.clientWidth !== undefined) {
            setWidth(densityChartMirrorSvgRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleDensityMirrorChart = (): void => {
        d3.select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.densityChartMirrorSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform',
                `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_doubleHist.csv').then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([-10, 15])
                .range([0, width]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .call(d3.axisBottom(x));

            const y1: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .range([height / 2, 0])
                .domain([0, 0.12]);

            svg.append('g')
                .attr('transform', 'translate(0, 0)')
                .call(d3.axisLeft(y1).tickValues([0.05, 0.1]));

            const y2: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .range([height / 2, height])
                .domain([0, 0.12]);

            svg.append('g')
                .attr('transform', 'translate(0, 0)')
                .call(d3.axisLeft(y2).ticks(2).tickSizeOuter(5));

            const kde: any = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(60));
            const densityA: any = kde(
                data.filter((d: d3.DSVRowString<string>) => d.type === 'variable 1')
                    .map((d: d3.DSVRowString<string>) => d.value)
            );

            const densityB: any = kde(
                data.filter((d: d3.DSVRowString<string>) => d.type === 'variable 2')
                    .map((d: d3.DSVRowString<string>) => d.value)
            );

            svg.append('path')
                .datum(densityA)
                .attr('class', 'mypath')
                .attr('stroke', colors.color_black)
                .attr('stroke-width', '0.6px')
                .attr('fill', colors.color_pink)
                .attr('opacity', '0.8')
                .attr('d', d3.line()
                    .curve(d3.curveBasis)
                    .x((d: [number, number]) => x(d[0]))
                    .y((d: [number, number]) => y1(d[1]))
                );

            svg.append('path')
                .datum(densityB)
                .attr('class', 'mypath')
                .attr('stroke', '#000')
                .attr('stroke-width', '0.6px')
                .attr('fill', colors.color_purple)
                .attr('opacity', '0.8')
                .attr('d', d3.line()
                    .curve(d3.curveBasis)
                    .x((d: [number, number]) => x(d[0]))
                    .y((d: [number, number]) => y2(d[1]))
                );

            svg.append('circle')
                .attr('cx', densityChartMirrorSvgRef.current?.clientWidth !== undefined ? densityChartMirrorSvgRef.current.clientWidth - 210 : 0)
                .attr('cy', '30')
                .attr('r', 6)
                .attr('fill', colors.color_pink);

            svg.append('text')
                .attr('x', densityChartMirrorSvgRef.current?.clientWidth !== undefined ? densityChartMirrorSvgRef.current.clientWidth - 180: 0)
                .attr('y', '30')
                .style('font-size', '15px')
                .style('alignment-baseline', 'middle')
                .text('variable 1');

            svg.append('circle')
                .attr('cx', densityChartMirrorSvgRef.current?.clientWidth !== undefined ? densityChartMirrorSvgRef.current.clientWidth - 210 : 0)
                .attr('cy', '60')
                .attr('r', 6)
                .attr('fill', colors.color_purple);

            svg.append('text')
                .attr('x', densityChartMirrorSvgRef.current?.clientWidth !== undefined ? densityChartMirrorSvgRef.current.clientWidth - 180 : 0)
                .attr('y', 60)
                .style('font-size', '15px')
                .style('alignment-baseline', 'middle')
                .text('variable 2');
        });
        
        const kernelDensityEstimator = (kernel: (v: number) => number, X: any): any => {
            return (V: any) => {
                return X.map((x: any) => {
                    return [x, d3.mean(V, (v: number) => {
                        return kernel(x - v);
                    })];
                });
            };
        };

        const kernelEpanechnikov = (k: number): any => {
            return (v: number): number => {
                return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
            };
        };
    };

    useEffect(() => {
        handleResize();
        handleDensityMirrorChart();

        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleDensityMirrorChart();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleDensityMirrorChart();
                }, 100);
            });
        };
    }, [handleDensityMirrorChart]);

    return (
        <div className='densityChartMirror'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='rounded-md'>
                            <div className='densityChartMirrorSvg' ref={densityChartMirrorSvgRef}>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DensityChartMirror;