import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './densityChartSidebar.sass';

const DensityChartSidebar: React.FC = (): ReactNode => {
    const densityChartSidebarRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 30, bottom: 30, left: 50 };

    const handleReSize = (): void => {
        if (densityChartSidebarRef.current?.clientWidth !== undefined) {
            setWidth(densityChartSidebarRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleDensityChartSidebar = useCallback((): void => {
        d3.select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.densityChartSidebarSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 -100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv').then((data: d3.DSVRowArray<string>) => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 1000])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .style('transform', `translate(0px, ${height}px)`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .range([height, 0])
                .domain([0.000, 0.010]);

            svg.append('g')
                .call(d3.axisLeft(y))
                .style('transform', `translate(0px, ${0}px)`);

            const kde: any = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(40));
            const density: any = kde(data.map((d: d3.DSVRowString<string>) => d.price));

            const curve: d3.Selection<SVGPathElement, any, HTMLElement, any> = svg.append('g')
                .append('path')
                .attr('class', 'mypath')
                .datum(density)
                .attr('stroke', colors.color_black)
                .attr('stroke-width', '0.6')
                .attr('fill', colors.color_pink)
                .attr('opacity', '0.8')
                .attr('d', d3.line()
                    .curve(d3.curveBasis)
                    .x((d: [number, number]) => x(d[0]))
                    .y((d: [number, number]) => y(d[1]))
                );

            d3.select('.mysidebar').on('change', (event) => {
                update(event.target.value);
            });

            const update = (bindNumber: any): void => {
                const kde: any = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(bindNumber));
                const density: any = kde(data.map((d: d3.DSVRowString<string>) => d.price));

                curve.datum(density)
                    .transition()
                    .duration(2000)
                    .attr('d', d3.line()
                        .curve(d3.curveBasis)
                        .x((d: [number, number]) => x(d[0]))
                        .y((d: [number, number]) => y(d[1]))
                    );
            };
        });
    }, [width, height]);

    const kernelDensityEstimator = (kernel: (v: number) => number, X: any): any => {
        return (V: any) => {
            return X.map((x: any) => {
                return [x, d3.mean(V, (v: number) => {
                    return kernel(x - v);
                })];
            });
        };
    };

    const kernelEpanechnikov = (k: number): any => {
        return (v: number): number => {
            return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
        };
    };

    useEffect(() => {
        handleReSize();
        handleDensityChartSidebar();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.addEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleDensityChartSidebar]);

    return (
        <div className='densityChartSidebar'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='rounded-md'>
                            <div className='densityChartSidebarSvg' ref={densityChartSidebarRef}>
                                <div className='search_container'>
                                    <input className='mysidebar' type='range' name='sidebar' min='10' max='100' />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DensityChartSidebar;