import React, { useEffect, useState, useRef, useCallback, ChangeEvent, ReactNode } from 'react';
import * as d3 from 'd3';

// css
import './densityChartDropdown.sass';
import colors from '@/styles/colors.sass';

const DensityChartDropdown: React.FC = (): ReactNode => {
    const densityChartDropdownSvgRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };
    
    const handleResize = (): void => {
        if (densityChartDropdownSvgRef.current?.clientWidth !== undefined) {
            setWidth(densityChartDropdownSvgRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleDensityChartDropdown = useCallback((): void => {
        d3.select('.densityChartDropdownSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.densityChartDropdownSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.right})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/iris.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 12])
                .range([0, width]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .call(d3.axisBottom(x));

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .range([height, 0])
                .domain([0.00, 0.40]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const kde: any = kernelDensityEstimator(kernelEpanechnikov(3), x.ticks(140));
            const density: any = kde(
                data.filter((d: d3.DSVRowString<string>) => d.Species === 'setosa')
                    .map((d: d3.DSVRowString<string>) => d.Sepal_Length)
            );

            const curve: d3.Selection<SVGPathElement, any, HTMLElement, any> = svg.append('path')
                .datum(density)
                .attr('stroke', colors.color_black)
                .attr('stroke-width', '0.6')
                .attr('fill', colors.color_blue)
                .attr('opacity', '0.5')
                .attr('d', d3.line()
                    .curve(d3.curveBasis)
                    .x((d: [number, number]) => x(d[0]))
                    .y((d: [number, number]) => y(d[1]))
                );

            d3.select('select').on('change', (event: ChangeEvent<HTMLSelectElement>) => {
                update(event.target.value);
            });

            const update = (selectedGroup: string): void => {
                const density = kde(
                    data.filter((d: d3.DSVRowString<string>) => d.Species === selectedGroup)
                        .map((d: d3.DSVRowString<string>) => d.Sepal_Length)
                );

                curve.datum(density)
                    .transition()
                    .duration(2000)
                    .attr('d', d3.line()
                        .curve(d3.curveBasis)
                        .x((d: [number, number]) => x(d[0]))
                        .y((d: [number, number]) => y(d[1]))
                    );
            };
        });
    }, [width, height]);

    const kernelDensityEstimator = (kernel: (v: number) => number, X: any): any => {
        return (V: any) => {
            return X.map((x: any) => {
                return [x, d3.mean(V, (v: number) => {
                    return kernel(x - v);
                })];
            });
        };
    };

    const kernelEpanechnikov = (k: number): any => {
        return (v: number): number => {
            return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
        };
    };

    useEffect(() => {
        handleResize();
        handleDensityChartDropdown();

        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleResize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleResize();
                }, 100);
            });
        };
    }, [handleDensityChartDropdown]);

    return (
        <div className='densityChartDropdown'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='rounded-md'>
                            <div className='densityChartDropdownSvg' ref={densityChartDropdownSvgRef}>
                                <div className='search_container'>
                                    <select className='sepal_option'>
                                        <option value='setosa'>setosa</option>
                                        <option value='versicolor'>versicolor</option>
                                        <option value='virginica'>virginica</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DensityChartDropdown;