import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './densityChartBasic.sass';

const DensityBasic: React.FC = (): ReactNode => {
    const densityChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };

    const handleReSize = (): void => {
        if (densityChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(densityChartBasicRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleDensityChart = useCallback((): void => {
        d3.select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.densityChartBasicSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            // 繪製 x 軸
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 1000])
                .range([0, width]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .call(d3.axisBottom(x))
                .attr('stroke-width', 0.6);

            // 繪製 y 軸
            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .range([height, 0])
                .domain([0, 0.01]);

            svg.append('g')
                .call(d3.axisLeft(y))
                .attr('stroke-width', 0.6);

            const kde: any = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(40));
            const density: any = kde(data.map((d: d3.DSVRowString<string>) => d.price));

            svg.append('path')
                .attr('class', 'myPath')
                .datum(density)
                .attr('fill', colors.color_purple)
                .attr('opacity', '0.8')
                .attr('stroke', colors.color_black)
                .attr('stroke-width', 0.6)
                .attr('stroke-linejoin', 'round')
                .attr('d', d3.line()
                    .curve(d3.curveBasis)
                    .x((d: [number, number]) => x(d[0]))
                    .y((d: [number, number]) => y(d[1]))
                );

            // 遮罩密度動畫
            svg.append('rect')
                .attr('width', width)
                .attr('height', height)
                .attr('x', 0)
                .attr('fill', colors.color_white)
                .transition()
                .duration(2000)
                .ease(d3.easeLinear)
                .attr('x', width);
        });
    }, [width, height]);

    const kernelDensityEstimator = (kernel: (v: number) => number, X: any): any => {
        return (V: any) => {
            return X.map((x: any) => {
                return [x, d3.mean(V, (v: number) => kernel(x - v))];
            });
        };
    };

    const kernelEpanechnikov = (k: number): any => {
        return (v: number): number => {
            return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
        };
    };

    useEffect(() => {
        handleReSize();
        handleDensityChart();

        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return (): void => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleDensityChart]);

    return (
        <div className='densityChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='rounded-md'>
                            <div className='densityChartBasicSvg' ref={densityChartBasicRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DensityBasic;