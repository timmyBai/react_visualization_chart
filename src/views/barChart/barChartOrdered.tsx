import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './barChartOrdered.sass';
import colors from '@/styles/colors.sass';

const BarChartOrdered: React.FC = (): ReactNode => {
    const barChartOrderedRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const handleReSize = (): void => {
        if (barChartOrderedRef.current?.clientWidth !== undefined) {
            setWidth(barChartOrderedRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBarChartOrdered = useCallback((): void => {
        d3.select('.barChartOrderedSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.barChartOrderedSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/7_OneCatOneNum_header.csv').then((data: d3.DSVRowArray<string>) => {
            data = data.sort((a: d3.DSVRowString<string>, b: d3.DSVRowString<string>) => {
                return parseInt(b.Value) - parseInt(a.Value);
            });

            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(data.map((d: d3.DSVRowString<string>) => d.Country))
                .range([0, width])
                .padding(0.2);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`)
                .selectAll('text')
                .attr('transform', 'translate(-10, 0) rotate(-45)')
                .style('text-anchor', 'end')
                .style('font-size', '12px');

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 13000])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y))
                .selectAll('text')
                .attr('font-size', '12px');

            svg.selectAll('rect')
                .data(data)
                .enter()
                .append('rect')
                .attr('width', x.bandwidth())
                .attr('x', (d: d3.DSVRowString<string>) => x(d.Country) as number)
                .attr('y', height)
                .attr('height', 0)
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number) => {
                    return 30 * i;
                })
                .ease(d3.easeElastic)
                .attr('y', (d: d3.DSVRowString<string>) => y(+d.Value))
                .attr('fill', colors.color_purple)
                .attr('height', (d: d3.DSVRowString<string>) => height - y(+d.Value));
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleBarChartOrdered();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleBarChartOrdered]);

    return (
        <div className='barChartOrdered'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='barChartOrderedSvg' ref={barChartOrderedRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BarChartOrdered;