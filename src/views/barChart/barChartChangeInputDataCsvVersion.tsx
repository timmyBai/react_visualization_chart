import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './barChartChangeInputDataCsvVersion.sass';

const BarChartChangeInputDataCsvVersion: React.FC = (): ReactNode => {
    const barChartChangeInputDataCsvVersionRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const handleReSize = (): void => {
        if (barChartChangeInputDataCsvVersionRef.current?.clientWidth !== undefined) {
            setWidth(barChartChangeInputDataCsvVersionRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBarChartChangeInputDataCsvVersion = useCallback((selectVar: string, barColor: string): void => {
        d3.select('.barChartChangeInputDataCsvVersionSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.barChartChangeInputDataCsvVersionSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/barplot_change_data.csv').then((data) => {
            const x = d3.scaleBand()
                .domain(data.map((d: any) => d.group))
                .range([0, width])
                .padding(0.2);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y = d3.scaleLinear()
                .domain([0, d3.max(data, (d: any) => +d[selectVar]) as any])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.selectAll('rect')
                .data(data)
                .enter()
                .append('rect')
                .attr('fill', barColor)
                .attr('width', x.bandwidth())
                .attr('x', (d: any) => x(d.group) as any)
                .attr('y', height)
                .attr('height', 0)
                .transition()
                .duration(2000)
                .delay((d: any, i: number) => i * 30)
                .ease(d3.easeElastic)
                .attr('y', (d: any) => y(d[selectVar]) as any)
                .attr('height', (d: any) => height - y(d[selectVar]) as any);
        });
    }, [width, height]);

    const handleChangeData1 = (): void => {
        handleBarChartChangeInputDataCsvVersion('var1', colors.color_purple);
    };

    const handleChangeData2 = (): void => {
        handleBarChartChangeInputDataCsvVersion('var2', colors.color_pink);
    };

    useEffect(() => {
        handleReSize();
        handleBarChartChangeInputDataCsvVersion('var1', colors.color_purple);
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [handleBarChartChangeInputDataCsvVersion]);

    return (
        <div className='barChartChangeInputDataCsvVersion'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className="card">
                            <button className='btn btnColorPurple' onClick={handleChangeData1}>Change data 1</button>
                            <button className='btn btnColorPink' onClick={handleChangeData2}>Change data 2</button>
                            
                            <div className='barChartChangeInputDataCsvVersionSvg' ref={barChartChangeInputDataCsvVersionRef}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BarChartChangeInputDataCsvVersion;