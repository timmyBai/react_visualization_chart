import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './barChartChangeInputDataUpgrade.sass';
import colors from '@/styles/colors.sass';

const BarChartChangeInputDataUpgrade: React.FC = (): ReactNode => {
    const barChartChangeInputDataUpgradeRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };
    const data1: BarChartChangeInputDataUpgradeType[] = [
        { group: 'A', value: 4 },
        { group: 'B', value: 16 },
        { group: 'C', value: 8 }
    ];

    const data2: BarChartChangeInputDataUpgradeType[] = [
        { group: 'A', value: 7 },
        { group: 'B', value: 1 },
        { group: 'C', value: 20 },
        { group: 'D', value: 10 }
    ];

    const handleReSize = (): void => {
        if (barChartChangeInputDataUpgradeRef.current?.clientWidth !== undefined) {
            setWidth(barChartChangeInputDataUpgradeRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBarChartChangeInputData = useCallback((data: BarChartChangeInputDataUpgradeType[]) => {
        d3.select('.barChartChangeInputDataUpgradeSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.barChartChangeInputDataUpgradeSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        const x: d3.ScaleBand<string> = d3.scaleBand()
            .domain(data.map((d: BarChartChangeInputDataUpgradeType) => d.group))
            .range([0, width])
            .padding(0.2);

        svg.append('g')
            .attr('transform', `translate(0, ${height})`)
            .transition()
            .duration(2000)
            .call(d3.axisBottom(x));

        const y = d3.scaleLinear()
            .domain([0, d3.max(data, (d: BarChartChangeInputDataUpgradeType) => d.value) as number])
            .range([height, 0]);

        svg.append('g')
            .transition()
            .duration(1000)
            .call(d3.axisLeft(y));

        svg.selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('fill', colors.color_purple)
            .attr('width', x.bandwidth())
            .attr('x', (d: BarChartChangeInputDataUpgradeType) => x(d.group) as number)
            .attr('y', height)
            .attr('height', 0)
            .transition()
            .duration(2000)
            .delay((d: BarChartChangeInputDataUpgradeType, i: number) => i * 60)
            .ease(d3.easeElastic)
            .attr('y', (d: BarChartChangeInputDataUpgradeType) => y(d.value) as number)
            .attr('height', (d: BarChartChangeInputDataUpgradeType) => height - y(d.value) as number);
    }, [width, height]);

    const handleChangeData1 = (): void => {
        handleBarChartChangeInputData(data1);
    };

    const handleChangeData2 = (): void => {
        handleBarChartChangeInputData(data2);
    };

    useEffect(() => {
        handleReSize();
        handleBarChartChangeInputData(data1);
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleBarChartChangeInputData]);

    return (
        <div className='barChartChangeInputDataUpgrade'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className="card">
                            <button className='btn btnColorBlue' onClick={handleChangeData1}>Change data 1</button>
                            <button className='btn btnColorPurple' onClick={handleChangeData2}>Change data 2</button>

                            <div className='barChartChangeInputDataUpgradeSvg' ref={barChartChangeInputDataUpgradeRef}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BarChartChangeInputDataUpgrade;