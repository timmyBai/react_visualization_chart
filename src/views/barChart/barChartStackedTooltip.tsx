import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './barChartStackedTooltip.sass';

const BarChartStackedTooltip: React.FC = (): ReactNode => {
    const barChartStackedTooltipRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 40, left: 55 };

    const handleReSize = (): void => {
        if (barChartStackedTooltipRef.current?.clientWidth !== undefined) {
            setWidth(barChartStackedTooltipRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBarChartStackedTooltip = useCallback((): void => {
        d3.select('.barChartStackedTooltipSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.barChartStackedTooltipSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_stacked.csv').then((data: d3.DSVRowArray<string>) => {
            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(data.map((d: d3.DSVRowString<string>) => d.group))
                .range([0, width])
                .padding(0.2);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 60])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const subGroups: string[] = data.columns.slice(1);

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .domain(subGroups)
                .range([colors.color_blue, colors.color_purple, colors.color_pink]);

            const stackedData: d3.Series<{
                [key: string]: number;
            }, string>[] = d3.stack().keys(subGroups)(data as any);

            svg.append('g')
                .selectAll('g')
                .data(stackedData)
                .enter()
                .append('g')
                .attr('fill', (d: d3.Series<{
                    [key: string]: number;
                }, string>) => color(d.key) as string)
                .selectAll('rect')
                .data((d: d3.Series<{
                    [key: string]: number;
                }, string>) => d)
                .enter()
                .append('rect')
                .attr('x', (d: any) => x(d.data.group) as number)
                .attr('width', x.bandwidth())
                .attr('y', height)
                .attr('height', 0)
                .attr('stroke', colors.color_grey)
                .attr('stroke-width', 1)
                .transition()
                .duration(2000)
                .delay((d: d3.SeriesPoint<{
                    [key: string]: number;
                }>, i: number) => i * 30)
                .ease(d3.easeElastic)
                .attr('y', (d: d3.SeriesPoint<{
                    [key: string]: number;
                }>) => y(d[1]))
                .attr('height', (d: d3.SeriesPoint<{
                    [key: string]: number;
                }>) => y(d[0]) - y(d[1]));

            d3.select('.barChartStackedTooltipSvg').select('.tooltip').remove();

            const mouseOver = (event: MouseEvent, d: any): void => {
                if (event.target instanceof SVGElement) {
                    const subgroupName = d3.select(event.target.parentNode as SVGRectElement).datum() as any;
                    const subgroupNameKey = subgroupName?.key;
                    const subgroupValue = d.data[subgroupName?.key];
    
                    d3.select('.barChartStackedTooltipSvg')
                        .append('div')
                        .attr('class', 'tooltip')
                        .style('background-color', colors.color_black)
                        .style('color', colors.color_white)
                        .style('padding', '5px 10px')
                        .style('border-radius', '5px')
                        .style('position', 'absolute')
                        .style('display', 'inline-block')
                        .style('left', '5px')
                        .style('opacity', 0)
                        .html(`
                            <p>SubGroups: ${subgroupNameKey}</p>
                            <p>Value: ${subgroupValue}</p>
                        `).transition()
                        .duration(100)
                        .style('opacity', 1);
                }
            };

            const mouseMove = (event: React.MouseEvent<SVGAElement>): void => {
                const svgContainer = barChartStackedTooltipRef.current;
                if (!svgContainer) return;

                d3.select('.barChartStackedTooltipSvg')
                    .select('.tooltip')
                    .style('left', `${event.pageX + 3}px`)
                    .style('top', `${event.pageY + 3}px`);
            };

            const mouseLeave = (): void => {
                d3.select('.barChartStackedTooltipSvg').select('.tooltip').remove();
            };

            svg.selectAll('rect').on('mouseover', mouseOver);
            svg.selectAll('rect').on('mousemove', mouseMove);
            svg.selectAll('rect').on('mouseleave', mouseLeave);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleBarChartStackedTooltip();
        window.addEventListener('resize', () => {
            setTimeout(() => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', () => {
                setTimeout(() => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleBarChartStackedTooltip]);

    return (
        <div className='barChartStackedTooltip'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='barChartStackedTooltipSvg' ref={barChartStackedTooltipRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BarChartStackedTooltip;