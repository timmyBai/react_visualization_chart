import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './barChartPercentStacked.sass';
import colors from '@/styles/colors.sass';

const BarChartPercentStacked: React.FC = (): ReactNode => {
    const barChartPercentStackedRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const handleReSize = (): void => {
        if (barChartPercentStackedRef.current?.clientWidth !== undefined) {
            setWidth(barChartPercentStackedRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBarChartPercentStacked = useCallback((): void => {
        d3.select('.barChartPercentStackedSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.barChartPercentStackedSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_stacked.csv').then((data: d3.DSVRowArray<string>) => {
            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(data.map((d: d3.DSVRowString<string>) => d.group))
                .range([0, width])
                .padding(0.2);

            svg.append('g')
                .call(d3.axisBottom(x).tickSizeOuter(0))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 100])
                .range([height, 0]);
                
            svg.append('g')
                .call(d3.axisLeft(y));

            const subGroups: string[] = data.columns.slice(1);

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .domain(subGroups)
                .range([colors.color_blue, colors.color_pink, colors.color_purple]);

            data.forEach((d: any) => {
                let total: number = 0;

                for(const i in subGroups) {
                    const name = subGroups[i];
                    total += +d[name];
                }

                for(const i in subGroups) {
                    const name = subGroups[i];
                    d[name] = d[name] / total * 100;
                }
            });

            const stackData: d3.Series<{
                [key: string]: number;
            }, string>[] = d3.stack()
                .keys(subGroups)(data as any);

            svg.append('g')
                .selectAll('g')
                .data(stackData)
                .enter()
                .append('g')
                .attr('fill', (d: d3.Series<{
                    [key: string]: number;
                }, string>) => color(d.key) as number)
                .selectAll('rect')
                .data((d: d3.Series<{
                    [key: string]: number;
                }, string>) => d)
                .enter()
                .append('rect')
                .attr('x', (d: any) => x(d.data.group) as number)
                .attr('width', x.bandwidth())
                .attr('y', height)
                .attr('height', 0)
                .transition()
                .duration(2000)
                .delay((d: d3.SeriesPoint<{
                    [key: string]: number;
                }>, i: number) => i * 30)
                .ease(d3.easeElastic)
                .attr('y', (d: d3.SeriesPoint<{
                    [key: string]: number;
                }>) => y(d[1]))
                .attr('height', (d: d3.SeriesPoint<{
                    [key: string]: number;
                }>) => y(d[0]) - y(d[1]));
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleBarChartPercentStacked();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleBarChartPercentStacked]);

    return (
        <div className='barChartPercentStacked'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='barChartPercentStackedSvg' ref={barChartPercentStackedRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BarChartPercentStacked;