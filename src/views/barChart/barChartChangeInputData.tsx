import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './barChartChangeInputData.sass';

const BarChartChangeInputData: React.FC = (): ReactNode => {
    const barChartChangeInputDataRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const data1: BarChartChangeInputDataType[] = [
        { group: 'A', value: 4 },
        { group: 'B', value: 16 },
        { group: 'C', value: 8 }
    ];

    const data2: BarChartChangeInputDataType[] = [
        { group: 'A', value: 7 },
        { group: 'B', value: 1 },
        { group: 'C', value: 20 }
    ];

    const handleReSize = (): void => {
        if (barChartChangeInputDataRef.current?.clientWidth !== undefined) {
            setWidth(barChartChangeInputDataRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBarChartChangeInputData = useCallback((data: BarChartChangeInputDataType[]): void | Promise<void> => {
        d3.select('.barChartChangeInputDataSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.barChartChangeInputDataSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        const x: d3.ScaleBand<string> = d3.scaleBand()
            .domain(data.map((d: BarChartChangeInputDataType) => d.group))
            .range([0, width])
            .padding(0.2);

        svg.append('g')
            .attr('transform', `translate(0, ${height})`)
            .call(d3.axisBottom(x));

        const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
            .domain([0, d3.max(data, (d: BarChartChangeInputDataType) => d.value) as number])
            .range([height, 0]);

        svg.append('g')
            .call(d3.axisLeft(y));

        svg.selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('fill', colors.color_blue)
            .attr('x', (d: BarChartChangeInputDataType) => x(d.group) as number)
            .attr('width', x.bandwidth())
            .attr('y', height)
            .attr('height', 0)
            .transition()
            .duration(2000)
            .delay((d: BarChartChangeInputDataType, i: number) => i * 30)
            .ease(d3.easeElastic)
            .attr('y', (d: BarChartChangeInputDataType) => y(d.value) as number)
            .attr('height', (d: BarChartChangeInputDataType) => height - y(d.value));
    }, [width, height]);

    const handleChangeData1 = (): void => {
        handleBarChartChangeInputData(data1);
    };

    const handleChangeData2 = (): void => {
        handleBarChartChangeInputData(data2);
    };

    useEffect(() => {
        handleReSize();
        handleBarChartChangeInputData(data1);

        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleBarChartChangeInputData]);

    return (
        <div className='barChartChangeInputData'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className="card">
                            <button className='btn btnColorBlue' onClick={handleChangeData1}>Change data 1</button>
                            <button className='btn btnColorPink' onClick={handleChangeData2}>Change data 2</button>
                            
                            <div className='barChartChangeInputDataSvg' ref={barChartChangeInputDataRef}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BarChartChangeInputData;