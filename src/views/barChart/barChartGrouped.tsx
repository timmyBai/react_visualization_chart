import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './barChartGrouped.sass';
import colors from '@/styles/colors.sass';

const BarChartGrouped: React.FC = (): ReactNode => {
    const barChartGroupedRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 45, bottom: 55, left: 45 };

    const handleReSize = (): void => {
        if (barChartGroupedRef.current?.clientWidth !== undefined) {
            setWidth(barChartGroupedRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBarChartGrouped = useCallback((): void => {
        d3.select('.barChartGroupedSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.barChartGroupedSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_stacked.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(data.map((d: d3.DSVRowString<string>) => d.group))
                .range([0, width])
                .padding(0.2);

            svg.append('g')
                .call(d3.axisBottom(x).tickSizeOuter(0))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 40])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const subGroups: string[] = data.columns.slice(1);

            const xSubGroup: d3.ScaleBand<string> = d3.scaleBand()
                .domain(subGroups)
                .range([0, x.bandwidth()])
                .padding(0.05);

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .domain(subGroups)
                .range([colors.color_blue, colors.color_purple, colors.color_pink]);

            svg.append('g')
                .selectAll('g')
                .data(data)
                .enter()
                .append('g')
                .attr('transform', (d: d3.DSVRowString<string>) => `translate(${x(d.group)}, 0)`)
                .selectAll('rect')
                .data((d: d3.DSVRowString<string>) => {
                    return subGroups.map((key: string) => {
                        return {
                            key: key,
                            value: d[key]
                        };
                    });
                })
                .enter()
                .append('rect')
                .attr('width', xSubGroup.bandwidth())
                .attr('x', (d: BarChartGroupedKeyDataType) => xSubGroup(d.key) as number)
                .attr('fill', (d: BarChartGroupedKeyDataType) => color(d.key) as string)
                .attr('y', height)
                .attr('height', 0)
                .transition()
                .duration(2000)
                .delay((d: BarChartGroupedKeyDataType, i: number) => i * 30)
                .ease(d3.easeElastic)
                .attr('y', (d: BarChartGroupedKeyDataType) => y(+d.value))
                .attr('height', (d: BarChartGroupedKeyDataType) => height - y(+d.value));
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleBarChartGrouped();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleBarChartGrouped]);

    return (
        <div className='barChartGrouped'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='barChartGroupedSvg' ref={barChartGroupedRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BarChartGrouped;