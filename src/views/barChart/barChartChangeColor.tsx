import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './barChartChangeColor.sass';
import colors from '@/styles/colors.sass';

const BarChartChangeColor: React.FC = (): ReactNode => {
    const barChartChangeColorRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const handleResize = (): void => {
        if (barChartChangeColorRef.current?.clientWidth !== undefined) {
            setWidth(barChartChangeColorRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleBarChartChangeColor = useCallback((): void => {
        d3.select('.barChartChangeColorSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.barChartChangeColorSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/7_OneCatOneNum_header.csv').then((data: d3.DSVRowArray<string>) => {
            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(data.map((d: d3.DSVRowString<string>) => d.Country))
                .range([0, width])
                .padding(0.2);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`)
                .selectAll('text')
                .attr('font-size', '12px');

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 13000])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y))
                .selectAll('text')
                .attr('font-size', '12px');

            svg.selectAll('rect')
                .data(data)
                .enter()
                .append('rect')
                .attr('fill', colors.color_pink)
                .attr('x', (d: d3.DSVRowString<string>) => x(d.Country) as number)
                .attr('width', x.bandwidth())
                .attr('y', height)
                .attr('height', 0)
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number) => 30 * i)
                .ease(d3.easeElastic)
                .attr('y', (d: d3.DSVRowString<string>) => y(+d.Value))
                .attr('height', (d: d3.DSVRowString<string>) => height - y(+d.Value));
        });
    }, [width, height]);

    const changeBarChartBlueColor = (): void => {
        d3.select('.barChartChangeColorSvg')
            .select('svg')
            .selectAll('rect')
            .transition()
            .duration(2000)
            .delay((d: unknown, i: number) => 50 * i)
            .ease(d3.easeElastic)
            .attr('fill', colors.color_blue);
    };

    const changeBarChartPurpleColor = (): void => {
        d3.select('.barChartChangeColorSvg')
            .select('svg')
            .selectAll('rect')
            .transition()
            .duration(2000)
            .delay((d: unknown, i: number) => 50 * i)
            .ease(d3.easeElastic)
            .attr('fill', colors.color_purple);
    };

    useEffect(() => {
        handleResize();
        handleBarChartChangeColor();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleResize();
            }, 100);
        });

        return () => {
            window.addEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleResize();
                }, 100);
            });
        };
    }, [handleBarChartChangeColor]);

    return (
        <div className='barChartChangeColor'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            <button className='btn btnColorBlue' onClick={changeBarChartBlueColor}>Color blue</button>
                            <button className='btn btnColorPurple' onClick={changeBarChartPurpleColor}>Color purple</button>

                            <div className='barChartChangeColorSvg' ref={barChartChangeColorRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BarChartChangeColor;