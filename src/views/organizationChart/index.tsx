import React, { ReactNode, useEffect } from 'react';
import * as d3 from 'd3';
import { linkVertical } from 'd3';

// css
import './index.sass';
import colors from '@/styles/colors.sass';

// images
import orgnoAddIcon from '@/assets/images/organization/orgnoAdd.svg';
import orgnoVisableIcon from '@/assets/images/organization/orgnoVisable.svg';
import orgnoEditIcon from '@/assets/images/organization/orgnoEdit.svg';

interface TreeNode {
    name: string;
    enable: boolean;
    children?: TreeNode[];
}

const OrganizationChart: React.FC = (): ReactNode => {
    const data: TreeNode = {
        'name': 'flare',
        'enable': true,
        'children': [
            {
                'name': 'analytics',
                'enable': true,
                'children': [
                    {
                        'name': 'cluster',
                        'enable': true,
                        'children': [
                            {
                                'name': 'AgglomerativeCluster',
                                'enable': true
                            },
                            {
                                'name': 'CommunityStructure',
                                'enable': true
                            }
                        ]
                    },
                    {
                        'name': 'graph',
                        'enable': true,
                        'children': [
                            {
                                'name': 'BetweennessCentrality',
                                'enable': true
                            },
                            {
                                'name': 'LinkDistance',
                                'enable': true
                            },
                            {
                                'name': 'MaxFlowMinCut',
                                'enable': true
                            }
                        ]
                    }
                ]
            },
            {
                'name': 'animate',
                'enable': true,
                'children': [
                    {
                        'name': 'Easing',
                        'enable': true
                    },
                    {
                        'name': 'FunctionSequence',
                        'enable': true
                    },
                    {
                        'name': 'interpolate',
                        'enable': true,
                        'children': [
                            {
                                'name': 'ArrayInterpolator',
                                'enable': true
                            },
                            {
                                'name': 'ColorInterpolator',
                                'enable': true
                            },
                            {
                                'name': 'DateInterpolator',
                                'enable': true
                            },
                            {
                                'name': 'Interpolator',
                                'enable': true
                            }
                        ]
                    },
                    {
                        'name': 'ISchedulable',
                        'enable': true
                    },
                    {
                        'name': 'Parallel',
                        'enable': true
                    }
                ]
            },
            {
                'name': 'data',
                'enable': true,
                'children': [
                    {
                        'name': 'converters',
                        'enable': true,
                        'children': [
                            {
                                'name': 'Converters',
                                'enable': true
                            }
                        ]
                    },
                    {
                        'name': 'DataField',
                        'enable': true
                    },
                    {
                        'name': 'DataSchema',
                        'enable': true
                    },
                    {
                        'name': 'DataSet',
                        'enable': true
                    }
                ]
            }
        ]
    };

    const handleOrganizationChart = (): void => {
        const tree = (data: TreeNode): d3.HierarchyPointNode<TreeNode> => {
            const root = d3.hierarchy(data, (d) => d.children);
            return d3.tree<TreeNode>().nodeSize([250, 240])(root);
        };

        const root = tree(data);

        // js
        const width = 100;
        const height = 100;
        const { body } = document;
        const translateX = body.getBoundingClientRect().width / 2.5;
        const translateY = body.getBoundingClientRect().height / 5;
        const boxHeight = 1;

        d3.select('svg').remove();

        const svg = d3.select('.organizationChartSvg')
            .append('svg')
            .attr('width', `${width}%`)
            .attr('height', `${height}vh`)
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '24px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white);

        const g = svg.append('g')
            .attr('class', 'treeGroup');

        const linkGenerator = linkVertical<TreeNode, d3.HierarchyPointNode<TreeNode>>()
            .x(d => d.x)
            .y(d => d.y);

        g.append('g')
            .attr('fill', 'none')
            .attr('stroke', '#ced4da')
            .attr('stroke-width', 1)
            .attr('transform', `translate(${translateX}, ${translateY})`)
            .selectAll('path')
            .data(root.links() as any) // Note the "as any" cast
            .join('path')
            .attr('d', (d: any) => linkGenerator(d) as string);

        // 組織卡片
        g.append('g')
            .attr('stroke-width', 3)
            .selectAll('g')
            .data(root.descendants())
            .join('g')
            .append('rect')
            .attr('transform', (d: any) => `translate(${d.x - 100 + translateX}, ${d.y - 50 + translateY})`)
            .attr('width', 210)
            .attr('height', 110)
            .attr('fill', '#ffffff')
            .attr('stroke', '#ced4da')
            .attr('stroke-width', '1')
            .attr('rx', '6');

        // 組織功能群組
        const toolGroup = g.append('g')
            .attr('stroke-width', 1)
            .selectAll('g')
            .data(root.descendants())
            .join('g')
            .attr('transform', (d: any) => `translate(${d.x - 100 + translateX}, ${d.y - 50 + translateY})`);

        // 功能區域特殊處理
        toolGroup
            .append('defs')
            .append('clipPath')
            .attr('id', 'round-corner')
            .append('rect')
            .attr('width', 210)
            .attr('height', 40)
            .attr('rx', '6')
            .attr('ry', '6');

        // 組織功能方塊區域
        toolGroup
            .append('rect')
            .attr('width', 210)
            .attr('height', 30)
            .attr('clip-path', 'url(#round-corner)')
            .attr('fill', colors.color_dark_purple);

        // 新增組織按鈕
        toolGroup
            .append('image')
            .attr('href', orgnoAddIcon)
            .attr('width', 30)
            .attr('height', 30)
            .style('cursor', 'pointer')
            .on('click', () => {
                alert('add');
            });

        // 隱藏組織按鈕
        toolGroup
            .append('image')
            .attr('href', orgnoVisableIcon)
            .attr('class', 'imgAdd')
            .attr('width', 30)
            .attr('height', 30)
            .attr('transform', 'translate(30)')
            .style('cursor', 'pointer')
            .on('click', () => {
                alert('visable');
            });

        // 編輯組織按鈕
        toolGroup
            .append('image')
            .attr('href', orgnoEditIcon)
            .attr('width', 30)
            .attr('height', 30)
            .attr('transform', 'translate(60)')
            .style('cursor', 'pointer')
            .on('click', function () {
                alert('update');
            });

        function drawLine(): void {
            const link = svg.selectAll('path')
                .data(root.links());

            link.enter().append('path')
                .attr('class', 'link');

            link.exit().remove();

            link.attr('d', d => elbow(d));

            function elbow(d: any): string {
                const sourceX = d.source.x,
                    sourceY = d.source.y + boxHeight,
                    targetX = d.target.x,
                    targetY = d.target.y;

                return `
                        M${sourceX},${sourceY}
                        V${(targetY - sourceY) / 2 + sourceY}
                        H${targetX}
                        V${targetY}
                    `;
            }
        }

        drawLine();

        // Zoom 事件讓整個群組可以移動放大縮小
        const zoom = d3.zoom()
            .scaleExtent([0.5, 5])
            .on('zoom', function (event) {
                g.attr('transform', event.transform);
            });
        svg.call(zoom as any);
    };

    useEffect(() => {
        handleOrganizationChart();
    }, []);


    return (
        <div className='organizationChart'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='organizationChartSvg'></div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OrganizationChart;