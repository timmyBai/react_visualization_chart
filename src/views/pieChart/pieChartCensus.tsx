import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// files
import pieChartCensus from '@/assets/files/pieChartCensus.csv';

// css
import colors from '@/styles/colors.sass';
import './pieChartCensus.sass';

const PieChartCensus: React.FC = (): ReactNode => {
    const pieChartCensusRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const handleReSize = (): void => {
        if (pieChartCensusRef.current?.clientWidth !== undefined) {
            setWidth(pieChartCensusRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handlePieChartCensus = useCallback(() => {
        d3.select('.pieChartCensusSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.pieChartCensusSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height * 1.3})`);

        d3.csv(pieChartCensus as any).then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const pie: d3.Pie<any, number | {
                valueOf(): number;
            }> = d3.pie()
                .sort(null)
                .value((d: any) => d[1].value);

            const data_ready: d3.PieArcDatum<number | {
                valueOf(): number;
            }>[] = pie(Object.entries(data) as any);

            const radius: number = Math.min(width, height) / 2;

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .range(d3.schemeDark2);

            const arcGenerator: d3.Arc<any, any> = d3.arc<any>()
                .innerRadius(0)
                .outerRadius(radius * 2);

            svg.append('g')
                .selectAll('path')
                .data(data_ready)
                .enter()
                .append('path')
                .attr('fill', (d: any): string => color(d.data[0]) as string)
                .attr('d', arcGenerator);

            svg.append('g')
                .selectAll('text')
                .data(data_ready)
                .enter()
                .append('text')
                .style('font-size', '10px')
                .style('font-weight', '600')
                .attr('transform', (d: any): string => {
                    const pos1 = arcGenerator.centroid(d)[0] * 1.5 - 10;
                    const pos2 = arcGenerator.centroid(d)[1] * 1.5;

                    return `translate(${pos1}, ${pos2})`;
                })
                .text((d: any): string => {
                    return d.data[1].name;
                });

            svg.append('g')
                .selectAll('text')
                .data(data_ready)
                .enter()
                .append('text')
                .style('font-size', '10px')
                .attr('transform', (d: any): string => {
                    const pos1 = arcGenerator.centroid(d)[0] * 1.5 - 20;
                    const pos2 = arcGenerator.centroid(d)[1] * 1.5 + 15;

                    return `translate(${pos1}, ${pos2})`;
                })
                .text((d: any): number => {
                    return d.data[1].value;
                });
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handlePieChartCensus();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handlePieChartCensus]);

    return (
        <div className='pieChartCensus'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            <div className='pieChartCensusSvg' ref={pieChartCensusRef}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PieChartCensus;