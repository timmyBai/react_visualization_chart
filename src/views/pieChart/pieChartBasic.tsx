import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './pieChartBasic.sass';

const PieChartBasic: React.FC = (): ReactNode => {
    const pieChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const handleReSize = (): void => {
        if (pieChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(pieChartBasicRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handlePieChartBasic = useCallback((): void => {
        d3.select('.pieChartBasicSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.pieChartBasicSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height})`);

        const radius: number = Math.min(width, height) / 2;

        const data: PieChartBasicDataType = {
            a: 9,
            b: 20,
            c: 30,
            d: 8,
            e: 12
        };

        const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
            .range([colors.color_black, colors.color_blue, colors.color_pink, colors.color_light_blue, colors.color_purple]);

        const pie: d3.Pie<any, number | {
            valueOf(): number;
        }> = d3.pie()
            .value((d: any) => d[1]);

        const data_ready: d3.PieArcDatum<number | {
            valueOf(): number;
        }>[] = pie(Object.entries(data) as any);

        svg.append('g')
            .selectAll('path')
            .data(data_ready)
            .enter()
            .append('path')
            .attr('d', d3.arc<any>()
                .innerRadius(0)
                .outerRadius(radius)
            )
            .attr('fill', (d: any): string => {
                return color(d.data[0]) as string;
            });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handlePieChartBasic();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handlePieChartBasic]);

    return (
        <div className='pieChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='pieChartBasicSvg' ref={pieChartBasicRef}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PieChartBasic;
