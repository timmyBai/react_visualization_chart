import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './pieChartInputDataSelector.sass';

const PieChartInputDataSelector: React.FC = (): ReactNode => {
    const pieChartInputDataSelectorRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 90, left: 55 };

    const data1: PieChartInputDataSelectorData1Type = { a: 9, b: 20, c: 30, d: 8, e: 12 };
    const data2: PieChartInputDataSelectorData2Type = { a: 6, b: 16, c: 20, d: 14, e: 19, f: 12 };

    const handleReSize = (): void => {
        if (pieChartInputDataSelectorRef.current?.clientWidth !== undefined) {
            setWidth(pieChartInputDataSelectorRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handlePieChartInputDataSelector = useCallback((data: PieChartInputDataSelectorData1Type | PieChartInputDataSelectorData2Type): void => {
        d3.select('.pieChartInputDataSelectorSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.pieChartInputDataSelectorSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height})`);

        const pie: d3.Pie<any, number | {
            valueOf(): number;
        }> = d3.pie()
            .value((d: any) => d[1]);

        const data_ready: d3.PieArcDatum<number | {
            valueOf(): number;
        }>[] = pie(Object.entries(data) as any);

        const radius: number = Math.min(width, height) / 2;
        
        const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
            .range([colors.color_blue, colors.color_pink, colors.color_purple, colors.color_light_pink, colors.color_black, colors.color_light_blue]);

        svg.append('g')
            .selectAll('path')
            .data(data_ready)
            .enter()
            .append('path')
            .attr('fill', (d: any): string => color(d) as string)
            .attr('d', d3.arc<any>()
                .innerRadius(0)
                .outerRadius(radius)
            );
    }, [width, height]);

    const handleChangeData1 = (): void => {
        handlePieChartInputDataSelector(data1);
    };

    const handleChangeData2 = (): void => {
        handlePieChartInputDataSelector(data2);
    };

    useEffect(() => {
        handleReSize();
        handlePieChartInputDataSelector(data1);
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handlePieChartInputDataSelector]);

    return (
        <div className='pieChartInputDataSelector'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            <button className='btn btnData1' onClick={handleChangeData1}>data 1</button>
                            <button className='btn btnData2' onClick={handleChangeData2}>data 2</button>
                            
                            <div className='pieChartInputDataSelectorSvg' ref={pieChartInputDataSelectorRef}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PieChartInputDataSelector;