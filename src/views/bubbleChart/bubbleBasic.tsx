import React, { ReactNode } from 'react';

// css
import './bubbleBasic.sass';

const BubbleBasic: React.FC = (): ReactNode => {
    return (
        <div className='bubble'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='bubbleBasicSvg'>
                            基礎氣泡圖
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BubbleBasic;