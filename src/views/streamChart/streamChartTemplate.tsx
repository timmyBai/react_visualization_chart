import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './streamChartTemplate.sass';

const StreamChartTemplate: React.FC = (): ReactNode => {
    const streamChartTemplateRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (streamChartTemplateRef.current?.clientWidth !== undefined) {
            setWidth(streamChartTemplateRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleStreamChartTemplate = useCallback((): void => {
        d3.select('.streamChartTemplateSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.streamChartTemplateSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/5_OneCatSevNumOrdered_wide.csv').then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(d3.extent(data, (d: d3.DSVRowString<string>): number => +d.year) as [number, number])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x).tickSize(-height * 0.7).tickPadding(10))
                .attr('transform', `translate(0, ${height * 0.8})`)
                .select('.domain').remove();

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([-100000, 100000])
                .range([height, 0]);

            const keys: string[] = data.columns.slice(1);

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .domain(keys)
                .range([
                    colors.color_blue,
                    colors.color_pink,
                    colors.color_light_pink,
                    colors.color_light_blue,
                    colors.color_purple,
                    colors.color_dark_purple,
                    colors.color_light_black,
                    colors.color_black
                ]);

            const stackedData: d3.Series<{
                [key: string]: number;
            }, string>[] = d3.stack()
                .offset(d3.stackOffsetSilhouette)
                .keys(keys)(data as any);
            
            // add x tick tag name
            svg.append('text')
                .attr('x', width - 100)
                .attr('y', height - 20)
                .text('Time(year)');

            // add tooltip
            const tooltip: d3.Selection<SVGTextElement, unknown, HTMLElement, any> = svg.append('text')
                .attr('x', 5)
                .attr('y', 5);

            const mouseOver = (e: React.MouseEvent<SVGPathElement>): void => {
                d3.selectAll('.myPath').style('opacity', 0.2);
                d3.select(e.target as SVGPathElement).style('opacity', 1);
                tooltip.style('opacity', 1);
            };

            const mouseMove = (e: React.MouseEvent<SVGPathElement>, d: any): void => {
                tooltip.text(d.key);
            };

            const mouseLeave = (): void => {
                d3.selectAll('.myPath').style('opacity', 1);
                tooltip.style('opacity', 0).text('');
            };

            svg.selectAll('myLayer')
                .data(stackedData)
                .enter()
                .append('path')
                .attr('class', 'myPath')
                .attr('fill', (d: any): string => color(d.key) as string)
                .attr('d', d3.area<any>()
                    .x((d: any): number => x(d.data.year))
                    .y0((d: any): number => y(d[0]))
                    .y1((d: any): number => y(d[1]))
                )
                .on('mouseover', mouseOver)
                .on('mousemove', mouseMove)
                .on('mouseleave', mouseLeave);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleStreamChartTemplate();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return (): void => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleStreamChartTemplate]);

    return (
        <div className='streamChartTemplate'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='streamChartTemplateSvg' ref={streamChartTemplateRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default StreamChartTemplate;