import React, { ReactNode } from 'react';

// css
import './spiderChartBasic.sass';

const SpiderChartBasic: React.FC = (): ReactNode => {
    return (
        <div className='spiderChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='rounded-md'>
                            <div className='spiderChartBasicSvg'>
                                基礎蜘蛛圖
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SpiderChartBasic;