import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartSmallMultiple.sass';

const LineChartSmallMultiple: React.FC = (): ReactNode => {
    const lineChartSmallMultipleRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (lineChartSmallMultipleRef.current?.clientWidth !== undefined) {
            setWidth(lineChartSmallMultipleRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 4 - margin.top - margin.bottom);
        }
    };

    const handleLineChartSmallMultiple = useCallback((): void => {
        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/5_OneCatSevNumOrdered.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            d3.select('.lineChartSmallMultipleSvg').selectAll('.uniqueChart').remove();

            const sumstat: d3.InternMap<any, d3.DSVRowString<string>[]> = d3.group(data, (d: d3.DSVRowString<string>) => d.name);

            const svg: d3.Selection<SVGGElement, [any, d3.DSVRowString<string>[]], d3.BaseType, unknown> = d3.selectAll('.lineChartSmallMultipleSvg')
                .selectAll('.uniqueChart')
                .data(sumstat)
                .enter()
                .append('div')
                .attr('class', 'uniqueChart w-full sm:w-6/12 md:w-6/12 lg:w-4/12 xl:w-4/12 xxl-w-4/12')
                .style('height', '25vh')
                .style('overflow', 'hidden')
                .append('svg')
                .attr('width', '100%')
                .attr('height', '25vh')
                .attr('viewbox', '-50 -50 100 100')
                .style('background-color', colors.color_white)
                .append('g')
                .attr('transform', `translate(${margin.left}, ${margin.top})`);

            const svgWidth: number = document.querySelector('svg')?.clientWidth || 0;
            const svgHeight: number = document.querySelector('svg')?.clientHeight || 0;

            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(d3.extent(data, (d: d3.DSVRowString<string>) => parseInt(d.year)) as [number, number])
                .range([0, svgWidth - margin.left - margin.right]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .attr('width', '100%')
                .call(d3.axisBottom(x).ticks(3));

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(data, (d: d3.DSVRowString<string>) => +d.n)] as [number, number])
                .range([svgHeight - margin.top - margin.bottom, 0]);

            svg.append('g')
                .call(d3.axisLeft(y).ticks(5));

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .range(d3.schemeSet2);

            svg.append('path')
                .attr('stroke', (d: [any, d3.DSVRowString<string>[]]): string => color(d[0]) as string)
                .attr('fill', 'none')
                .attr('d', (d: [any, d3.DSVRowString<string>[]]) => d3.line<any>()
                    .x((d: any) => x(d.year))
                    .y((d: any) => y(+d.n))(d[1])
                );

            svg.append('text')
                .attr('text-anchor', 'start')
                .attr('x', 0)
                .attr('y', -10)
                .attr('fill', (d: any) => color(d[0]) as string)
                .text((d: [any, d3.DSVRowString<string>[]]) => d[0] as string);

            svg.append('rect')
                .attr('x', 1)
                .attr('width', width)
                .attr('height', height)
                .attr('fill', colors.color_white)
                .transition()
                .duration(10000)
                .attr('transform', `translate(${width}, 0)`);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleLineChartSmallMultiple();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleLineChartSmallMultiple]);

    return (
        <div className='lineChartSmallMultiple'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='lineChartSmallMultipleSvg' ref={lineChartSmallMultipleRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartSmallMultiple;