import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartBasic.sass';

const LineChartBasic: React.FC = (): ReactNode => {
    const lineChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (lineChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(lineChartBasicRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartBasic = useCallback((): void => {
        d3.select('.lineBasicSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineBasicSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const parseData: LineChartBasicCsvDataType[] = [];

            data.forEach((d: d3.DSVRowString<string>) => {
                parseData.push({
                    date: d3.timeParse('%Y-%m-%d')(d.date) as Date,
                    value: d.value
                });
            });

            const x: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(d3.extent(parseData, d => d.date) as [Date, Date])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(data, (d) => +d.value) as number])
                .range([height, 0]);
            
            svg.append('g')
                .call(d3.axisLeft(y));

            svg.append('path')
                .datum(parseData)
                .attr('fill', 'none')
                .attr('stroke', colors.color_blue)
                .attr('stroke-width', 1.5)
                .attr('d', d3.line<any>()
                    .x((d) => x(d.date))
                    .y((d) => y(d.value))
                );

            svg.append('rect')
                .attr('x', 1)
                .attr('width', width)
                .attr('height', height)
                .attr('fill', colors.color_white)
                .transition()
                .duration(8000)
                .attr('transform', `translate(${width}, 0)`);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleLineChartBasic();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleLineChartBasic]);

    return (
        <div className='lineBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='lineBasicSvg' ref={lineChartBasicRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartBasic;