import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './lineChartFilterGroupDropDown.sass';

const LineChartFilterGroupDropDown: React.FC = (): ReactNode => {
    const lineChartFilterGroupDropDownRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleResize = (): void => {
        if (lineChartFilterGroupDropDownRef.current?.clientWidth !== undefined) {
            setWidth(lineChartFilterGroupDropDownRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartFilterGroupDropDown = useCallback((): void => {
        d3.select('.lineChartFilterGroupDropDownSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineChartFilterGroupDropDownSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/5_OneCatSevNumOrdered.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(d3.extent(data, (d: d3.DSVRowString<string>): number => +d.year) as [number, number])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(data, (d: d3.DSVRowString<string>) => +d.n) as number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .range(d3.schemeSet2);

            const allGroup: Set<string> = new Set(data.map(d => d.name));

            d3.select('.data_mode')
                .selectAll('option')
                .data(allGroup)
                .enter()
                .append('option')
                .attr('value', (d: string) => d)
                .text((d: string) => d);

            svg.append('path')
                .datum(data.filter((d: d3.DSVRowString<string>) => d.name === 'Helen'))
                .attr('class', 'myPath')
                .attr('fill', 'none')
                .attr('stroke', color('Helen') as string)
                .attr('d', d3.line<any>()
                    .x((d: any) => x(d.year))
                    .y((d: any) => y(+d.n))
                );

            d3.select('.data_mode').on('change', (event: React.ChangeEvent<HTMLSelectElement>): void => {
                d3.select('.lineChartFilterGroupDropDownSvg').select('svg').select('path.myPath').remove();

                svg.append('path')
                    .datum(data.filter((d: d3.DSVRowString<string>): boolean => d.name === event.target.value))
                    .attr('class', 'myPath')
                    .attr('fill', 'none')
                    .attr('stroke', color(event.target.value) as string)
                    .attr('d', d3.line<any>()
                        .x((d: any): number => x(d.year))
                        .y((d: any): number => y(+d.n))
                    );
            });
        });
    }, [width, height]);

    useEffect(() => {
        handleResize();
        handleLineChartFilterGroupDropDown();
        window.addEventListener('resize', () => {
            handleResize();
        });

        return () => {
            window.addEventListener('resize', () => {
                handleResize();
            });
        };
    }, [handleLineChartFilterGroupDropDown]);

    return (
        <div className='lineChartFilterGroupDropDown'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            <div className='search_container'>
                                <select className='data_mode'></select>
                            </div>

                            <div className='lineChartFilterGroupDropDownSvg' ref={lineChartFilterGroupDropDownRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartFilterGroupDropDown;