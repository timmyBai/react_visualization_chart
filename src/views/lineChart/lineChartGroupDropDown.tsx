import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartGroupDropDown.sass';

const LineChartGroupDropDown: React.FC = (): ReactNode => {
    const lineChartGroupDropDownRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (lineChartGroupDropDownRef.current?.clientWidth !== undefined) {
            setWidth(lineChartGroupDropDownRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartGroupDropDown = useCallback((): void => {
        d3.select('.lineChartGroupDropDownSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineChartGroupDropDownSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_connectedscatter.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 10])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 20])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .range([colors.color_blue, colors.color_purple, colors.color_pink]);

            const columns: string[] = data.columns.slice(1) as string[];

            d3.select('.data_mode')
                .selectAll('option')
                .data(columns)
                .enter()
                .append('option')
                .attr('value', (d: string) => d)
                .text((d: string) => d);

            svg.append('path')
                .datum(data)
                .attr('class', 'myPath')
                .attr('stroke', color('valueA') as string)
                .attr('fill', 'none')
                .attr('d', d3.line<any>()
                    .x((d: any) => x(+d.time))
                    .y((d: any) => y(+d.valueA))
                );

            d3.select('.data_mode').on('change', (event: React.ChangeEvent<HTMLSelectElement>) => {
                const dataFilter = data.map((d: d3.DSVRowString<string>): { time: string, value: string } => {
                    return {
                        time: d.time,
                        value: d[event.target.value]
                    };
                });

                svg.select('path.myPath').remove();

                svg.append('path')
                    .datum(dataFilter)
                    .attr('class', 'myPath')
                    .attr('stroke', color(event.target.value) as string)
                    .attr('fill', 'none')
                    .attr('d', d3.line<any>()
                        .x((d: any) => x(+d.time))
                        .y((d: any) => y(+d.value))
                    );
            });
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleLineChartGroupDropDown();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleLineChartGroupDropDown]);

    return (
        <div className='lineChartGroupDropDown'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className="card">
                            <div className='search_container'>
                                <select className='data_mode'></select>
                            </div>

                            <div className='lineChartGroupDropDownSvg' ref={lineChartGroupDropDownRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartGroupDropDown;