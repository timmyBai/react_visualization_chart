import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartGrouped.sass';

const LineChartGrouped: React.FC = (): ReactNode => {
    const lineChartGroupedRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (lineChartGroupedRef.current?.clientWidth !== undefined) {
            setWidth(lineChartGroupedRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartGrouped = useCallback((): void => {
        d3.select('.lineChartGroupedSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineChartGroupedSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/5_OneCatSevNumOrdered.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(d3.extent(data, (d: d3.DSVRowString<string>) => +d.year) as [number, number])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x).ticks(5))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(data, (d) => +d.n) as number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .range([
                    colors.color_pink,
                    colors.color_purple,
                    colors.color_grey,
                    colors.color_black,
                    colors.color_blue,
                    colors.color_dark_purple,
                    colors.color_light_pink,
                    colors.color_light_blue,
                    colors.color_light_black
                ]);

            const sumstat: d3.InternMap<string, d3.DSVRowString<string>[]> = d3.group(data, (d: d3.DSVRowString<string>): string => d.name);

            svg.selectAll('.line')
                .data(sumstat)
                .enter()
                .append('path')
                .attr('fill', 'none')
                .attr('stroke', (d: [string, d3.DSVRowString<string>[]]) => color(d[0]) as string)
                .attr('d', (d: [string, d3.DSVRowString<string>[]]) => d3.line<any>()
                    .x((d: any) => x(d.year))
                    .y((d: any) => y(+d.n))(d[1])
                );

            svg.append('rect')
                .attr('width', width)
                .attr('height', height)
                .attr('fill', colors.color_white)
                .attr('transform', 'translate(0, 0)')
                .transition()
                .duration(10000)
                .attr('transform', `translate(${width}, 0)`);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleLineChartGrouped();
        window.addEventListener('resize', () => {
            setTimeout(() => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', () => {
                setTimeout(() => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleLineChartGrouped]);

    return (
        <div className='lineChartGrouped'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='lineChartGroupedSvg' ref={lineChartGroupedRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartGrouped;