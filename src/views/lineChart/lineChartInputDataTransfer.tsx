import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartInputDataTransfer.sass';

const LineChartInputDataTransfer: React.FC = (): ReactNode => {
    const lineChartInputDataTransferRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 20, bottom: 35, left: 20 };
    const data1: LineChartInputDataTransferType[] = [
        {
            ser1: 0.3,
            ser2: 4
        },
        {
            ser1: 2,
            ser2: 16
        },
        {
            ser1: 3,
            ser2: 8
        }
    ];

    const data2: LineChartInputDataTransferType[] = [
        {
            ser1: 1,
            ser2: 7
        },
        {
            ser1: 4,
            ser2: 1
        },
        {
            ser1: 6,
            ser2: 8
        }
    ];

    const handleReSize = (): void => {
        if (lineChartInputDataTransferRef.current?.clientWidth !== undefined) {
            setWidth(lineChartInputDataTransferRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartInputDataTransfer = useCallback((data: LineChartInputDataTransferType[]): void => {
        d3.select('.lineChartInputDataTransferSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineChartInputDataTransferSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
            .domain([0, d3.max(data, (d: LineChartInputDataTransferType) => d.ser1)] as [number, number])
            .range([0, width]);

        svg.append('g')
            .call(d3.axisBottom(x))
            .attr('transform', `translate(0, ${height})`);

        const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
            .domain([0, d3.max(data, (d: LineChartInputDataTransferType) => d.ser2)] as [number, number])
            .range([height, 0]);

        svg.append('g')
            .call(d3.axisLeft(y));

        svg.append('path')
            .datum(data)
            .transition()
            .duration(3000)
            .attr('fill', 'none')
            .attr('stroke', colors.color_blue)
            .attr('d', d3.line<any>()
                .x((d: any): number => x(d.ser1))
                .y((d: any): number => y(d.ser2))
            );
    }, [width, height]);

    const handleChangeData1 = (): void => {
        handleLineChartInputDataTransfer(data1);
    };

    const handleChangeData2 = (): void => {
        handleLineChartInputDataTransfer(data2);
    };

    useEffect(() => {
        handleReSize();
        handleLineChartInputDataTransfer(data1);
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleLineChartInputDataTransfer]);

    return (
        <div className='lineChartInputDataTransfer'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='card'>
                            <button className='btn btnChangeData1' onClick={handleChangeData1}>Change data 1</button>
                            <button className='btn btnChangeData2' onClick={handleChangeData2}>Change data 2</button>

                            <div className='lineChartInputDataTransferSvg' ref={lineChartInputDataTransferRef}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartInputDataTransfer;