import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartCursorShowingExactValue.sass';

const LineChartCursorShowingExactValue: React.FC = (): ReactNode => {
    const lineChartCursorShowingExactValueRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 40 };

    const handleReSize = (): void => {
        if (lineChartCursorShowingExactValueRef.current?.clientWidth !== undefined) {
            setWidth(lineChartCursorShowingExactValueRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartCursorShowingExactValue = useCallback((): void => {
        d3.select('.lineChartCursorShowingExactValueSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineChartCursorShowingExactValueSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_IC.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 100])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 13])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const focus: d3.Selection<SVGCircleElement, unknown, HTMLElement, any> = svg.append('circle')
                .attr('r', 8.5)
                .attr('fill', 'none')
                .attr('stroke', colors.color_blue)
                .attr('opacity', 0);

            const focusText: d3.Selection<SVGTextElement, unknown, HTMLElement, any> = svg.append('text')
                .attr('x', 15)
                .attr('y', 5)
                .attr('opacity', 0)
                .text('x: 0, y: 0');

            svg.append('path')
                .datum(data)
                .attr('fill', 'none')
                .attr('stroke', colors.color_blue)
                .attr('d', d3.line<any>()
                    .x((d: any) => x(d.x))
                    .y((d: any) => y(d.y))
                );

            const bisector: (array: ArrayLike<any>, x: any, lo?: number | undefined, hi?: number | undefined) => number = d3.bisector((d: any) => d.x).left;

            const mouseOver = (): void => {
                focus.attr('cx', 0)
                    .attr('cy', 0)
                    .attr('opacity', 1);

                focusText.attr('x', 0)
                    .attr('y', 0)
                    .attr('opacity', 1);
            };

            const mouseMove = (event: MouseEvent): void => {
                const x0 = x.invert(d3.pointer(event)[0]);
                const i = bisector(data, x0, 1);
                const selection = data[i];

                focus.attr('cx', x(selection.x as any))
                    .attr('cy', y(selection.y as any));

                if (x(selection.x as any) < width / 2) {
                    focusText.attr('x', x((selection.x) as any) + 15)
                        .attr('y', y(selection.y as any) + 5)
                        .text(`x: ${selection.x}, y: ${selection.y}`);
                }
                else {
                    focusText.attr('x', x((selection.x) as any) - 110)
                        .attr('y', y(selection.y as any) + 5)
                        .text(`x: ${selection.x}, y: ${selection.y}`);
                }
            };

            const mouseout = (): void => {
                focus.attr('cx', 0)
                    .attr('cy', 0)
                    .attr('opacity', 0);

                focusText.attr('x', 0)
                    .attr('y', 0)
                    .attr('opacity', 0);
            };

            svg.append('rect')
                .attr('width', width)
                .attr('height', height)
                .attr('pointer-events', 'all')
                .attr('fill', 'none')
                .on('mouseover', mouseOver)
                .on('mousemove', mouseMove)
                .on('mouseout', mouseout);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleLineChartCursorShowingExactValue();
        window.addEventListener('resize', () => {
            setTimeout(() => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', () => {
                setTimeout(() => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleLineChartCursorShowingExactValue]);

    return (
        <div className='lineChartCursorShowingExactValue'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='lineChartCursorShowingExactValueSvg' ref={lineChartCursorShowingExactValueRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartCursorShowingExactValue;