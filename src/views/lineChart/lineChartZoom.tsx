import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartZoom.sass';

const LineChartZoom: React.FC = (): ReactNode => {
    const lineChartZoomRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (lineChartZoomRef.current?.clientWidth !== undefined) {
            setWidth(lineChartZoomRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartZoom = useCallback((): void => {
        d3.select('.lineChartZoomSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineChartZoomSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const parseData: LineChartZoom[] = [];

            data.forEach((d: d3.DSVRowString<string>) => {
                parseData.push({
                    date: d3.timeParse('%Y-%m-%d')(d.date) as Date,
                    value: +d.value
                });
            });

            const x: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(d3.extent(parseData, (d) => d.date) as [Date, Date])
                .range([0, width]);

            const xAxis: d3.Selection<SVGGElement, unknown, HTMLElement, any> = svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(parseData, (d: LineChartZoom): number => +d.value)] as [number, number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            // 新增切割器，切割折線圖邊緣
            svg.append('defs')
                .append('clipPath')
                .attr('id', 'clip')
                .append('rect')
                .attr('width', width)
                .attr('height', height)
                .attr('x', 0)
                .attr('y', 0);

            const line: d3.Selection<SVGPathElement, LineChartZoom[], HTMLElement, any> = svg.append('g')
                .attr('clip-path', 'url(#clip)')
                .append('path')
                .datum(parseData)
                .attr('fill', 'none')
                .attr('stroke', colors.color_purple)
                .attr('d', d3.line<any>()
                    .x((d: any): number => x(d.date))
                    .y((d: any): number => y(+d.value))
                );

            const updateChart = (e: any): void => {
                const extent = e.selection;

                if (extent) {
                    x.domain([x.invert(extent[0]), x.invert(extent[1])]);
    
                    xAxis.transition().duration(1000).call(d3.axisBottom(x));

                    svg.select('.brush').call(brushX.move as any, null);
    
                    line.datum(parseData)
                        .transition()
                        .duration(1000)
                        .attr('stroke', colors.color_purple)
                        .attr('d', d3.line<any>()
                            .x(d => x(d.date))
                            .y(d => y(+d.value))
                        );
                }
            };

            const brushX = d3.brushX()
                .extent([[0, 0], [width, height]])
                .on('end', updateChart);

            svg.append('g')
                .attr('class', 'brush')
                .call(brushX);

            svg.on('dblclick', () => {
                x.domain(d3.extent(parseData, (d) => d.date) as [Date, Date]);
                xAxis.transition().call(d3.axisBottom(x));

                line.datum(parseData).transition().duration(1000).attr('d', d3.line<any>().x(d => x(d.date)).y(d => y(d.value)) );
            });
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleLineChartZoom();
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [handleLineChartZoom]);

    return (
        <div className='lineChartZoom'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='lineChartZoomSvg' ref={lineChartZoomRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartZoom;