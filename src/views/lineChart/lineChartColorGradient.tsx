import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartColorGradient.sass';

const LineChartColorGradient: React.FC = (): ReactNode => {
    const lineChartColorGradientRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (lineChartColorGradientRef.current?.clientWidth !== undefined) {
            setWidth(lineChartColorGradientRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartColorGradient = useCallback((): void => {
        d3.select('.lineChartColorGradientSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineChartColorGradientSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const parseData: LineChartColorGradientCsvDataType[] = [];

            data.forEach((d: d3.DSVRowString<string>): void => {
                parseData.push({
                    date: d3.timeParse('%Y-%m-%d')(d.date) as Date,
                    value: d.value
                });
            });

            const x: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(d3.extent(parseData, (d) => d.date) as [Date, Date])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const max: number = d3.max(parseData, (d: LineChartColorGradientCsvDataType) => +d.value) as number;

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, max])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const linearGradientList: LinearGradient[] = [
                { offset: '0%', color: colors.color_blue },
                { offset: '50%', color: colors.color_pink },
                { offset: '100%', color: colors.color_purple }
            ];

            svg.append('linearGradient')
                .attr('id', 'line-gradient')
                .attr('gradientUnits', 'userSpaceOnUse')
                .attr('x1', 0)
                .attr('y1', y(0))
                .attr('x2', 0)
                .attr('y2', y(max))
                .selectAll('stop')
                .data(linearGradientList)
                .enter()
                .append('stop')
                .attr('offset', (d: LinearGradient) => d.offset)
                .attr('stop-color', (d: LinearGradient) => d.color);

            svg.append('path')
                .datum(parseData)
                .attr('fill', 'none')
                .attr('stroke', 'url(#line-gradient)')
                .attr('stroke-width', 1)
                .attr('d', d3.line<any>()
                    .x((d: any) => x(d.date))
                    .y((d: any) => y(d.value))
                );

            svg.append('rect')
                .attr('x', 1)
                .attr('width', width)
                .attr('height', height)
                .attr('fill', colors.color_white)
                .transition()
                .duration(10000)
                .attr('transform', `translate(${width}, 0)`);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleLineChartColorGradient();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return (): void => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleLineChartColorGradient]);

    return (
        <div className='lineChartColorGradient'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='lineChartColorGradientSvg' ref={lineChartColorGradientRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartColorGradient;