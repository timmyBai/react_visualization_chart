import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './lineChartConfidenceInterval.sass';

const LineChartConfidenceInterval: React.FC = (): ReactNode => {
    const lineChartConfidenceIntervalRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (lineChartConfidenceIntervalRef.current?.clientWidth !== undefined) {
            setWidth(lineChartConfidenceIntervalRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleLineChartConfidenceInterval = useCallback((): void => {
        d3.select('.lineChartConfidenceIntervalSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.lineChartConfidenceIntervalSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_IC.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([1, 100])
                .range([0, width]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .call(d3.axisBottom(x));

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 13])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.append('path')
                .datum(data)
                .attr('fill', colors.color_blue)
                .attr('opacity', 0.6)
                .attr('d', d3.area<any>()
                    .x((d: any) => x(d.x))
                    .y0((d: any) => y(d.CI_right))
                    .y1((d: any) => y(d.CI_left))
                );

            svg.append('path')
                .datum(data)
                .attr('fill', 'none')
                .attr('stroke', colors.color_blue)
                .attr('stroke-width', 2)
                .attr('d', d3.line<any>()
                    .x((d: any) => x(d.x))
                    .y((d: any) => y(d.y)));
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleLineChartConfidenceInterval();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleLineChartConfidenceInterval]);

    return (
        <div className='lineChartConfidenceInterval'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='lineChartConfidenceIntervalSvg' ref={lineChartConfidenceIntervalRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartConfidenceInterval;