import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './circularBarChartDouble.sass';

const CircularBarChartDouble: React.FC = (): ReactNode => {
    const circularBarChartDoubleRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);

    const handleReSize = (): void => {
        if (circularBarChartDoubleRef.current?.clientWidth !== undefined) {
            setWidth(circularBarChartDoubleRef.current.clientWidth);
            setHeight(window.innerHeight / 2);
        }
    };

    const handleCircularBarChartDouble = useCallback(() => {
        d3.select('.circularBarChartDoubleSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.circularBarChartDoubleSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/7_OneCatOneNum.csv').then((data) => {
            const innerRadius: number = 100;
            const outerRadius: number = Math.min(width, height) / 2;

            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(data.map(d => d.Country))
                .range([0, Math.PI * 2])
                .align(0);

            const y: d3.ScaleRadial<number, number, never> = d3.scaleRadial()
                .domain([0, 10000])
                .range([innerRadius, outerRadius]);

            // 縮小倍數
            const ybins: d3.ScaleRadial<number, number, never> = d3.scaleRadial()
                .domain([0, 10000])
                .range([innerRadius, 5]);

            svg.append('g')
                .selectAll('path')
                .data(data)
                .enter()
                .append('path')
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number): number => i * 30)
                .attr('fill', colors.color_pink)
                .attr('d', d3.arc<any>()
                    .innerRadius(innerRadius)
                    .outerRadius((d: CircularBarChartDoubleCsvDataType): number => y(d.Value))
                    .startAngle((d: CircularBarChartDoubleCsvDataType): number => x(d.Country) as number)
                    .endAngle((d: CircularBarChartDoubleCsvDataType): number => x(d.Country) as number + x.bandwidth())
                    .padAngle(0.01)
                    .padRadius(innerRadius)
                );

            svg.append('g')
                .selectAll('path')
                .data(data)
                .enter()
                .append('path')
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number) => i * 60)
                .attr('fill', colors.color_purple)
                .attr('d', d3.arc<any>()
                    .innerRadius(innerRadius)
                    .outerRadius((d: CircularBarChartDoubleCsvDataType): number => ybins(d.Value))
                    .startAngle((d: CircularBarChartDoubleCsvDataType): number => x(d.Country) as number)
                    .endAngle((d: CircularBarChartDoubleCsvDataType) => x(d.Country) as number + x.bandwidth())
                    .padAngle(0.01)
                    .padRadius(innerRadius)
                );

            svg.append('g')
                .selectAll('text')
                .data(data)
                .enter()
                .append('g')
                .attr('text-anchor', (d: d3.DSVRowString<string>) => {
                    return (x(d.Country) as number + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? 'end' : 'start';
                })
                .attr('transform', (d: d3.DSVRowString<string>): string => {
                    return `rotate(${(x(d.Country) as number + x.bandwidth() / 2) * 180 / Math.PI - 90}) translate(${y(+d.Value) + 10}, 0)`;
                })
                .append('text')
                .style('font-size', '11px')
                .attr('transform', (d: d3.DSVRowString<string>): string => {
                    return (x(d.Country) as number + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? 'rotate(180)' : 'rotate(0)';
                })
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number) => i * 30)
                .attr('alignment-baseline', 'middle')
                .text(d => d.Country);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleCircularBarChartDouble();
        window.addEventListener('resize', () => {
            setTimeout(() => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.addEventListener('resize', () => {
                setTimeout(() => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleCircularBarChartDouble]);

    return (
        <div className='circularBarChartDouble'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='circularBarChartDoubleSvg' ref={circularBarChartDoubleRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CircularBarChartDouble;