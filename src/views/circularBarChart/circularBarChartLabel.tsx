import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './circularBarChartLabel.sass';

const CircularBarChartLabel: React.FC = (): ReactNode => {
    const circularBarChartLabelRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);

    const handleReSize = (): void => {
        if (circularBarChartLabelRef.current?.clientWidth !== undefined) {
            setWidth(circularBarChartLabelRef.current.clientWidth);
            setHeight(window.innerHeight / 2);
        }
    };

    const handleCircularBarChartLabel = useCallback((): void => {
        const innerRadius: number = 100;
        const outerRadius: number = Math.min(width, height) / 2;

        d3.select('.circularBarChartLabelSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.circularBarChartLabelSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/7_OneCatOneNum.csv').then((data) => {
            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(data.map(d => d.Country))
                .range([0, Math.PI * 2]);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 10000])
                .range([innerRadius, outerRadius]);

            svg.append('g')
                .selectAll('path')
                .data(data)
                .enter()
                .append('path')
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number) => i * 30)
                .attr('fill', colors.color_blue)
                .attr('d', d3.arc<any>()
                    .innerRadius(innerRadius)
                    .outerRadius((d: CircularBarChartLabelCsvDataType): number => y(d.Value))
                    .startAngle((d: CircularBarChartLabelCsvDataType): number => x(d.Country) as number)
                    .endAngle((d: CircularBarChartLabelCsvDataType): number => x(d.Country) as number + x.bandwidth())
                    .padAngle(0.01)
                    .padRadius(innerRadius)
                );

            svg.append('g')
                .selectAll('text')
                .data(data)
                .enter()
                .append('g')
                .attr('text-anchor', (d: d3.DSVRowString<string>): string => {
                    return (x(d.Country) as number + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? 'end' : 'start';
                })
                .attr('transform', (d: d3.DSVRowString<string>): string => {
                    return `rotate(${(x(d.Country) as number + x.bandwidth() / 2) * 180 / Math.PI - 90}) translate(${y(+d.Value) + 10}, 0)`;
                })
                .append('text')
                .style('font-size', '11px')
                .attr('transform', (d: d3.DSVRowString<string>): string => {
                    return (x(d.Country) as number + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? 'rotate(180)' : 'rotate(0)';
                })
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number): number => i * 30)
                .attr('alignment-baseline', 'middle')
                .text((d: d3.DSVRowString<string>): string => d.Country);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleCircularBarChartLabel();
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [width, height]);

    return (
        <div className='circularBarChartLabel'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='circularBarChartLabelSvg' ref={circularBarChartLabelRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CircularBarChartLabel;