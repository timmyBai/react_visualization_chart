import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './circularBarChartStacked.sass';

const CircularBarChartStacked: React.FC = (): ReactNode => {
    const circularBarChartStackedRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);

    const handleReSize = (): void => {
        if (circularBarChartStackedRef.current?.clientWidth !== undefined) {
            setWidth(circularBarChartStackedRef.current.clientWidth);
            setHeight(window.innerHeight / 2);
        }
    };

    const handleCircularBarChartStacked = useCallback(() => {
        d3.select('.circularBarChartStackedSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.circularBarChartStackedSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .attr('font-size', '10')
            .append('g')
            .style('transform', 'translate(50%, 50%) scale(2)');

        d3.csv('https://gist.githubusercontent.com/mbostock/6fead6d1378d6df5ae77bb6a719afcb2/raw/30fcea31f2daed5023b9d3c6714fff48ee931606/data.csv').then((data): void | Promise<void> => {
            const parseData: CircularBarChartStackedCsvDataType[] = [];
            const innerRadius: number = 80;
            const outerRadius: number = Math.min(width, height) / 2;

            for (let i: number = 0; i < data.length; i++) {
                let total: number = 0;

                Object.keys(data).forEach((item, o) => {
                    const value = +data?.[i]?.[data.columns[o] as any] as unknown as number;
                    const isInteger = Number.isInteger(value);

                    total += isInteger ? value : 0;
                });

                parseData.push({
                    'State': data?.[i]?.State as unknown as string,
                    'Under 5 Years': data?.[i]?.['Under 5 Years'] as unknown as number,
                    '5 to 13 Years': data?.[i]?.['5 to 13 Years'] as unknown as number,
                    '14 to 17 Years': data?.[i]?.['14 to 17 Years'] as unknown as number,
                    '18 to 24 Years': data?.[i]?.['18 to 24 Years'] as unknown as number,
                    '25 to 44 Years': data?.[i]?.['25 to 44 Years'] as unknown as number,
                    '45 to 64 Years': data?.[i]?.['45 to 64 Years'] as unknown as number,
                    '65 Years and Over': data?.[i]?.['65 Years and Over'] as unknown as number,
                    total: total
                });
            }

            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(parseData.map(d => d.State))
                .range([0, Math.PI * 2])
                .align(0);

            const y: d3.ScaleRadial<number, number, never> = d3.scaleRadial()
                .domain([0, d3.max(parseData, d => d.total)] as [any, number])
                .range([innerRadius, outerRadius]);

            const z: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .domain(data.columns.slice(1))
                .range([
                    colors.color_blue,
                    colors.color_pink,
                    colors.color_purple,
                    colors.color_light_blue,
                    colors.color_light_pink,
                    colors.color_dark_purple,
                    colors.color_light_black
                ]);

            svg.append('g')
                .selectAll('g')
                .data(
                    d3.stack().keys(data.columns.slice(1))(parseData as any)
                )
                .enter()
                .append('g')
                .attr('fill', (d: d3.Series<{ [key: string]: number; }, string>) => z(d.key) as string)
                .selectAll('path')
                .data((d: d3.Series<{ [key: string]: number; }, string>) => d)
                .enter()
                .append('path')
                .transition()
                .duration(2000)
                .delay((d: d3.SeriesPoint<{ [key: string]: number; }>, i: number) => i * 30)
                .attr('d', d3.arc<any>()
                    .innerRadius((d: any): number => y(d[0]))
                    .outerRadius((d: any): number => y(d[1]))
                    .startAngle((d: any): number => x(d.data.State) as number)
                    .endAngle((d: any): number => x(d.data.State) as number + x.bandwidth())
                    .padAngle(0.01)
                    .padRadius(innerRadius)
                );

            // 標籤
            const label: d3.Selection<SVGGElement, d3.DSVRowString<string>, SVGGElement, unknown> = svg.append('g')
                .selectAll('g')
                .data(data)
                .enter()
                .append('g')
                .attr('text-anchor', 'middle')
                .attr('transform', (d: d3.DSVRowString<string>): string => {
                    return `rotate(${((x(d.State) as number + x.bandwidth() / 2) * 180 / Math.PI - 90)}) translate(${innerRadius}, 0)`;
                });

            label.append('text')
                .attr('transform', (d: d3.DSVRowString<string>): string => {
                    return (x(d.State) as number + x.bandwidth() / 2 + Math.PI / 2) % (2 * Math.PI) < Math.PI ? 'rotate(90) translate(0,12)' : 'rotate(-90) translate(0,-8)';
                })
                .style('font-size', '5px')
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number): number => i * 30)
                .attr('fill', colors.color_black)
                .text((d: d3.DSVRowString<string>): string => d.State);

            label.append('line')
                .attr('x2', -5)
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number) => i * 30)
                .attr('stroke', colors.color_black);

            // 軸線
            const yAxis: d3.Selection<SVGGElement, unknown, HTMLElement, any> = svg.append('g')
                .attr('text-anchor', 'middle');
            
            // 繪製刻度
            const yTick: d3.Selection<SVGGElement, number, SVGGElement, unknown> = yAxis
                .selectAll('g')
                .data(y.ticks(5).slice(1))
                .enter()
                .append('g');
            
            // 圓形刻度
            yTick.append('circle')
                .attr('r', y)
                .attr('fill', 'none')
                .attr('stroke', colors.color_black)
                .attr('stroke-width', 0.1)
                .attr('opacity', 0)
                .transition()
                .duration(2000)
                .delay((d: number, i: number): number => i * 30)
                .attr('opacity', 1);
            
            // 圓形刻度文字
            yTick.append('text')
                .attr('y', (d: number): number => -y(d))
                .attr('dy', '0.35em')
                .attr('fill', colors.color_black)
                .attr('stroke-width', 0.2)
                .style('font-size', '8px')
                .text(y.tickFormat(5, 's'))
                .attr('opacity', 0)
                .transition()
                .duration(2000)
                .delay((d: number, i: number) => i * 30)
                .attr('opacity', 1);
            
            // 標題
            yAxis.append('text')
                .attr('y', -y(y.ticks(5).pop() as number))
                .attr('dy', '-1em')
                .text('Population')
                .style('font-size', '8px')
                .attr('opacity', 0)
                .transition()
                .duration(2000)
                .delay((d: unknown, i: number) => i * 30)
                .attr('opacity', 1);

            // 說明
            const legend: d3.Selection<SVGGElement, string, SVGGElement, unknown> = svg
                .append('g')
                .style('transform', 'scale(2)')
                .selectAll('g')
                .data(data.columns.slice(1).reverse())
                .enter()
                .append('g')
                .attr('text-anchor', 'start')
                .attr('transform', (d: string, i: number) => {
                    return 'translate(-10,' + (i - (data.columns.length - 1) / 2) * 6 + ')';
                });
            
            // 標籤方塊
            legend.append('rect')
                .attr('width', 4)
                .attr('height', 4)
                .attr('fill', (d: string) => z(d) as string)
                .attr('opacity', 0)
                .transition()
                .duration(2000)
                .delay((d: string, i: number) => i * 30)
                .attr('opacity', 1);
            
            // 標籤文字
            legend.append('text')
                .attr('x', 8)
                .attr('y', 2)
                .attr('dy', '0.35em')
                .attr('font-size', '2px')
                .attr('opacity', 0)
                .transition()
                .duration(2000)
                .delay((d: string, i: number) => i * 30)
                .text((d: string) => d)
                .attr('opacity', 0)
                .transition()
                .duration(2000)
                .delay((d: unknown, i: number) => i * 30)
                .attr('opacity', 1);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleCircularBarChartStacked();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleCircularBarChartStacked]);

    return (
        <div className='circularBarChartStacked'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='circularBarChartStackedSvg' ref={circularBarChartStackedRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CircularBarChartStacked;