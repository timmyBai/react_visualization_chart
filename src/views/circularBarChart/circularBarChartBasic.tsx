import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './circularBarChartBasic.sass';

const CircularBarChartBasic: React.FC = (): ReactNode => {
    const circularBarChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    
    const handleReSize = (): void => {
        if (circularBarChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(circularBarChartBasicRef.current.clientWidth);
            setHeight(window.innerHeight / 2);
        }
    };

    const handleCircularBarChartBasic = useCallback(() => {
        const innerRadius: number = 80;
        const outerRadius: number = Math.min(width, height) / 2;

        d3.select('.circularBarChartBasicSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.circularBarChartBasicSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/7_OneCatOneNum.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleBand<string> = d3.scaleBand()
                .domain(data.map((d: d3.DSVRowString<string>): string => d.Country))
                .range([0, Math.PI * 2])
                .align(0)
                .padding(0.1);

            const y: d3.ScaleRadial<number, number, never> = d3.scaleRadial()
                .domain([0, 10000])
                .range([innerRadius, outerRadius]);

            svg.append('g')
                .selectAll('path')
                .data(data)
                .enter()
                .append('path')
                .transition()
                .duration(2000)
                .delay((d: d3.DSVRowString<string>, i: number): number => i * 30)
                .attr('fill', colors.color_purple)
                .attr('d', d3.arc<any>()
                    .innerRadius(innerRadius)
                    .outerRadius((d: CircularBarChartBasicCsvDataType): number => y(d.Value))
                    .startAngle((d: CircularBarChartBasicCsvDataType): number => x(d.Country) as number)
                    .endAngle((d: CircularBarChartBasicCsvDataType): number => x(d.Country) as number + x.bandwidth())
                    .padAngle(0.01)
                    .padRadius(innerRadius)
                );
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleCircularBarChartBasic();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleCircularBarChartBasic]);

    return (
        <div className='circularBarChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='circularBarChartBasicSvg' ref={circularBarChartBasicRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CircularBarChartBasic;