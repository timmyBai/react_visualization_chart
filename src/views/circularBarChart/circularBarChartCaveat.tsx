import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './circularBarChartCaveat.sass';

const CircularBarChartCaveat: React.FC = (): ReactNode => {
    const circularBarChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    
    const animation = (): void => {
        if (circularBarChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(circularBarChartBasicRef.current.clientWidth);
            setHeight(window.innerHeight);
        }

        requestAnimationFrame(animation);
    };

    const handleCircularBarChartCaveat = useCallback((): void => {
        d3.select('canvas').remove();

        const canvas: d3.Selection<HTMLCanvasElement, unknown, HTMLElement, any> = d3.select('.circularBarChartCanvasSvg')
            .append('canvas');
        const canvasNode: HTMLCanvasElement = canvas.node() as HTMLCanvasElement;
        const context: CanvasRenderingContext2D | null = canvasNode?.getContext('2d');

        const numSamplesPerFrame: number = 50;
        let numSamples: number = 0;

        canvasNode.width = width;
        canvasNode.height = height;

        const outerRadius: number = (Math.min(width, height) - 50) / 2;
        const innerRadius: number = outerRadius / 4;

        if (context && width && height) {
            context.translate(width / 2, height / 2);
            
            context.save();
            context.beginPath();
            context.arc(0, 0, outerRadius, 0, Math.PI * 2);
            context.moveTo(innerRadius, 0);
            context.arc(0, 0, innerRadius, Math.PI * 2, 0);
            context.fillStyle = 'rgba(0, 0, 0, 0.2)';
            context.fill('evenodd');
            context.restore();

            context.fillStyle = 'rgba(0, 0, 0, 0.5)';

            d3.timer(() => {
                for(let i: number = 0; i < numSamplesPerFrame; ++i) {
                    const r: number = innerRadius + Math.random() * (outerRadius - innerRadius);
                    const a: number = Math.random() * 2 * Math.PI;
                    const x: number = r * Math.cos(a);
                    const y: number = r * Math.sin(a);

                    context.beginPath();
                    context.arc(x, y, 1, 0, Math.PI * 2);
                    context.fill();
                }

                return ++numSamples > 1000;
            });
        }
    }, [width, height]);

    useEffect(() => {
        requestAnimationFrame(animation);

        handleCircularBarChartCaveat();
    }, [handleCircularBarChartCaveat]);

    return (
        <div className='circularBarChartCanvas'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='circularBarChartCanvasSvg' ref={circularBarChartBasicRef}>
                            <canvas></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CircularBarChartCaveat;