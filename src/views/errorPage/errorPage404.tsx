import React, { ReactNode } from 'react';

const ErrorPage404: React.FC = (): ReactNode => {
    return (
        <div className='errorPage'>
            404
        </div>
    );
};

export default ErrorPage404;