import React, { useEffect, useState, useRef, useCallback, ReactNode } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './histogramChartToolTip.sass';

const HistogramChartToolTip: React.FC = (): ReactNode => {
    const histogramChartToolTipRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };

    const handleResize = (): void => {
        if (histogramChartToolTipRef.current?.clientWidth !== undefined) {
            setWidth(histogramChartToolTipRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleHistogramChartToolTipChart = useCallback((): void => {
        d3.select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.histogramChartToolTipSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('background-color', colors.color_white)
            .attr('border-radius', '5px')
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 1000])
                .range([0, width]);

            svg.append('g').call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const histogram: d3.HistogramGeneratorNumber<any, number> = d3.bin<any, number>()
                .value((d: HistogramChartToolTipCsvDataType) => d.price)
                .domain([0, 1000])
                .thresholds(x.ticks(70));

            const bins: d3.Bin<any, number>[] = histogram(data);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .range([height, 0])
                .domain([0, d3.max(bins, (d: d3.Bin<any, number>) => d.length) as number]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.selectAll('rect')
                .data(bins)
                .enter()
                .append('rect')
                .attr('x', 1)
                .attr('transform', (d: d3.Bin<any, number>): string => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;

                    return `translate(${x(x1)}, ${height}) rotate(180)`;
                })
                .attr('fill', colors.color_blue)
                .attr('height', '0')
                .transition()
                .duration(1500)
                .delay((d: d3.Bin<any, number>, i: number): number => {
                    return 30 * i;
                })
                .ease(d3.easeElastic)
                .attr('width', (d: d3.Bin<any, number>) => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;
                    const x0 = d.x0 !== undefined ? d.x0 : 0;

                    if (x(x1) - x(x0) === 0) {
                        return x(x1) - x(x0);
                    }
                    else {
                        return `${x(x1) - x(x0) - 1}`;
                    }
                })
                .attr('height', (d: d3.Bin<any, number>) => {
                    return `${height - y(d.length)}`;
                });

            // tooltip
            d3.select('.histogramChartToolTipSvg').select('.tooltip').remove();

            const mouseOver = (event: MouseEvent, d: d3.Bin<any, number>): void => {
                const tooltip = d3.select('.histogramChartToolTipSvg')
                    .append('div')
                    .attr('class', 'tooltip')
                    .style('background-color', colors.color_black)
                    .style('color', colors.color_white)
                    .style('padding', '5px 10px')
                    .style('border-radius', '5px')
                    .style('position', 'absolute')
                    .style('display', 'inline-block')
                    .style('left', '5px')
                    .style('opacity', 0);

                tooltip
                    .html(`Range: ${d.x0} - ${d.x1}`)
                    .transition()
                    .duration(100)
                    .style('opacity', 1);
            };

            const mouseMove = (event: React.MouseEvent<SVGAElement>): void => {
                const svgContainer = histogramChartToolTipRef.current;
                if (!svgContainer) return;

                const rect = svgContainer.getBoundingClientRect();
                
                const offsetX = event.clientX - rect.left;
                const offsetY = event.clientY - rect.top;

                d3.select('.histogramChartToolTipSvg')
                    .select('.tooltip')
                    .style('left', `${offsetX + 3}px`)
                    .style('top', `${offsetY + 3}px`);
            };

            const mouseLeave = (): void => {
                d3.select('.histogramChartToolTipSvg').select('.tooltip').remove();
            };

            svg.selectAll<SVGGElement, d3.Bin<any, number>>('rect').on('mouseover', mouseOver);
            svg.selectAll('rect').on('mousemove', mouseMove);
            svg.selectAll('rect').on('mouseleave', mouseLeave);
        });
    }, [width, height]);

    useEffect(() => {
        handleResize();
        handleHistogramChartToolTipChart();

        window.addEventListener('resize', () => {
            setTimeout(() => {
                handleResize();
            }, 100);
        });

        return () => {
            window.addEventListener('resize', () => {
                setTimeout(() => {
                    handleResize();
                }, 100);
            });
        };
    }, [handleHistogramChartToolTipChart]);

    return (
        <div className='histogramChartToolTip'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='histogramChartToolTipSvg' ref={histogramChartToolTipRef}>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HistogramChartToolTip;