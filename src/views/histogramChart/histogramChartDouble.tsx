import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './histogramChartDouble.sass';

const HistogramChartDouble: React.FC = (): ReactNode => {
    const histogramChartDoubleRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };

    const handleResize = (): void => {
        if (histogramChartDoubleRef.current?.clientWidth !== undefined) {
            setWidth(histogramChartDoubleRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleHistogramChartDouble = useCallback((): void => {
        d3.select('.histogramChartColoredSvg').select('svg').remove();
        
        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.histogramChartColoredSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('background-color', colors.color_white)
            .attr('border-radius', '5px')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/data_doubleHist.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([-4, 9])
                .range([0, width]);
    
            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const histogram: d3.HistogramGeneratorNumber<any, number> = d3.bin<any, number>()
                .value((d) => +d.value)
                .domain(x.domain() as [number, number])
                .thresholds(x.ticks(40));

            const bins1: d3.Bin<any, number>[] = histogram(data.filter(d => d.type === 'variable 1'));
            const bins2: d3.Bin<any, number>[] = histogram(data.filter(d => d.type === 'variable 2'));

            const binMax1: number = d3.max(bins1, (d) => d.length) as number;
            const binMax2: number = d3.max(bins2, (d) => d.length) as number;

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, Math.max(binMax1, binMax2) as number])
                .range([height, 0]);
            
            svg.append('g')
                .call(d3.axisLeft(y));

            svg.selectAll('rect')
                .data(bins1)
                .enter()
                .append('rect')
                .attr('fill', colors.color_purple)
                .attr('opacity', 0.6)
                .attr('transform', (d: d3.Bin<any, number>): string => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;
                    
                    return `translate(${x(x1)}, ${height}) rotate(180)`;
                })
                .attr('width', (d: d3.Bin<any, number>): string => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;
                    const x0 = d.x0 !== undefined ? d.x0 : 0;

                    if (x(x1) - x(x0) === 0) {
                        return `${x(x1) - x(x0)}`;
                    }
                    else {
                        return `${x(x1) - x(x0) - 1}`;
                    }
                })
                .attr('height', 0)
                .transition()
                .duration(2000)
                .delay((d: d3.Bin<any, number>, i: number): number => i * 30)
                .ease(d3.easeElastic)
                .attr('height', (d: d3.Bin<any, number>): string => {
                    return `${height - y(d.length)}`;
                });

            svg.selectAll('rect2')
                .data(bins2)
                .enter()
                .append('rect')
                .attr('fill', colors.color_blue)
                .attr('opacity', 0.6)
                .attr('x', 1)
                .attr('transform', (d: d3.Bin<any, number>): string => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;

                    return `translate(${x(x1)}, ${height}) rotate(180)`;
                })
                .attr('width', (d: d3.Bin<any, number>) => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;
                    const x0 = d.x0 !== undefined ? d.x0 : 0;

                    if (x(x1) - x(x0) === 0) {
                        return `${x(x1) - x(x0)}`;
                    }
                    else {
                        return `${x(x1) - x(x0) - 1}`;
                    }
                })
                .attr('height', 0)
                .transition()
                .duration(2000)
                .delay((d: d3.Bin<any, number>, i: number): number => i * 30)
                .ease(d3.easeElastic)
                .attr('height', (d: d3.Bin<any, number>): string => {
                    return `${height - y(d.length)}`;
                });
        });
    }, [width, height]);

    useEffect(() => {
        handleResize();
        handleHistogramChartDouble();
        window.addEventListener('resize', (): void => {
            setTimeout((): void => {
                handleResize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleResize();
                }, 100);
            });
        };
    }, [handleHistogramChartDouble]);

    return (
        <div className='histogramChartDouble'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='histogramChartColoredSvg' ref={histogramChartDoubleRef}>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HistogramChartDouble;