import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './histogramChartBasic.sass';
import colors from '@/styles/colors.sass';

const HistogramChartBasic: React.FC = (): ReactNode => {
    const histogramChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };

    const handleResize = (): void => {
        if (histogramChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(histogramChartBasicRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleHistogramChartBasicChart = useCallback((): void => {
        d3.selectAll('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.histogramChartBasicSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('background-color', colors.color_white)
            .attr('border-radius', '5px')
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 1000])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const histogram: d3.HistogramGeneratorNumber<any, number> = d3.bin<any, number>()
                .value((d: HistogramChartBasicCsvDataType) => d.price)
                .domain([0, 1000])
                .thresholds(x.ticks(70));

            const bins: d3.Bin<any, number>[] = histogram(data);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .range([height, 0])
                .domain([0, d3.max(bins, (d: d3.Bin<any, number>) => d.length) as number]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.selectAll('rect')
                .data(bins)
                .join('rect')
                .attr('x', 1)
                .attr('transform', (d: d3.Bin<any, number>): string => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;

                    return `translate(${x(x1)}, ${height}) rotate(180)`;
                })
                .attr('fill', colors.color_pink)
                .attr('height', '0')
                .transition()
                .duration(1500)
                .delay((d: d3.Bin<any, number>, i: number): number => {
                    return 30 * i;
                })
                .ease(d3.easeElastic)
                .attr('width', (d: d3.Bin<any, number>): string => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;
                    const x0 = d.x0 !== undefined ? d.x0 : 0;

                    if (x(x1) - x(x0) === 0) {
                        return `${x(x1) - x(x0)}`;
                    }
                    else {
                        return `${x(x1) - x(x0) - 1}`;
                    }
                })
                .attr('height', (d: d3.Bin<any, number>): string => {
                    return `${height - y(d.length)}`;
                });
        });
    }, [width, height]);

    useEffect(() => {
        handleResize();
        handleHistogramChartBasicChart();

        window.addEventListener('resize', () => {
            setTimeout((): void => {
                handleResize();
            }, 100);
        });

        return (): void => {
            window.removeEventListener('resize', () => {
                setTimeout((): void => {
                    handleResize();
                }, 100);
            });
        };
    }, [handleHistogramChartBasicChart]);

    return (
        <div className='histogramChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='histogramChartBasicSvg' ref={histogramChartBasicRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HistogramChartBasic;