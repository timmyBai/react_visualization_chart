import React, { ReactNode, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './histogramChartColored.sass';
import colors from '@/styles/colors.sass';

const HistogramChartColored: React.FC = (): ReactNode => {
    const histogramChartColoredRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 30, right: 40, bottom: 50, left: 55 };

    const handleResize = (): void => {
        if (histogramChartColoredRef.current?.clientWidth !== undefined) {
            setWidth(histogramChartColoredRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleHistogramChartColored = (): void => {
        d3.select('.histogramChartColoredSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.histogramChartColoredSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('background-color', colors.color_white)
            .attr('border-radius', '5px')
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 1000])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const histogram: d3.HistogramGeneratorNumber<any, number> = d3.bin<any, number>()
                .value((d: HistogramChartColoredCsvType) => d.price)
                .domain(x.domain() as [number, number])
                .thresholds(x.ticks(70));

            const bins: d3.Bin<any, number>[] = histogram(data);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(bins, (d) => d.length) as number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const exponentialLine: number = 140;

            svg.selectAll('rect')
                .data(bins)
                .enter()
                .append('rect')
                .attr('x', 1)
                .attr('transform', (d: d3.Bin<any, number>): string => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;

                    return `translate(${x(x1)}, ${height}) rotate(180)`;
                })
                .attr('fill', (d: d3.Bin<any, number>): string => {
                    const x0 = d.x0 !== undefined ? d.x0 : 0;

                    if (x0 < exponentialLine) {
                        return colors.color_purple;
                    }

                    return colors.color_blue;
                })
                .attr('width', (d: d3.Bin<any, number>): string => {
                    const x1 = d.x1 !== undefined ? d.x1 : 0;
                    const x0 = d.x0 !== undefined ? d.x0 : 0;

                    if (x(x1) - x(x0) === 0) {
                        return `${x(x1) - x(x0)}`;
                    }
                    else {
                        return `${x(x1) - x(x0) - 1}`;
                    }
                })
                .attr('height', 0)
                .transition()
                .duration(2000)
                .delay((d: unknown, i: number): number => i * 30)
                .ease(d3.easeElastic)
                .attr('height', (d: d3.Bin<any, number>): string => {
                    return `${height - y(d.length)}`;
                });

            svg.append('line')
                .attr('x1', x(0))
                .attr('x2', x(0))
                .attr('y1', y(0))
                .attr('y2', 0)
                .attr('stroke', 'grey')
                .attr('stroke-width', 0.6)
                .transition()
                .duration(2000)
                .ease(d3.easeCircleInOut)
                .attr('x1', x(exponentialLine))
                .attr('x2', x(exponentialLine))
                .attr('y1', y(0))
                .attr('y2', y(1600))
                .attr('stroke-dasharray', '4');

            svg.append('text')
                .attr('x', x(0))
                .attr('y', y(1400))
                .transition()
                .duration(2000)
                .ease(d3.easeCircleInOut)
                .attr('x', x(exponentialLine + 10))
                .attr('y', y(1400))
                .text(`threshold: ${exponentialLine}`)
                .style('font-size', '15px');
        });
    };

    useEffect(() => {
        handleResize();
        handleHistogramChartColored();
        window.addEventListener('resize', (): void => {
            setTimeout(() => {
                handleResize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                setTimeout((): void => {
                    handleResize();
                }, 100);
            });
        };
    }, [width, height]);

    return (
        <div className='histogramChartColored'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='histogramChartColoredSvg' ref={histogramChartColoredRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HistogramChartColored;