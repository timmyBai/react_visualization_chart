import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './areaChartZoomWithFocus.sass';

const AreaChartZoomWithFocus: React.FC = (): ReactNode => {
    const areaChartZoomWithFocusRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const [height2, setHeight2] = useState<number>(0);
    const margin: MarginType = { top: 20, right: 20, bottom: 110, left: 40 };
    const margin2: MarginType = { top: 380, right: 20, bottom: 30, left: 40 };

    const handleReSize = (): void => {
        if (areaChartZoomWithFocusRef.current?.clientWidth !== undefined) {
            setWidth(areaChartZoomWithFocusRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
            setHeight2(window.innerHeight / 2 - margin2.top - margin2.bottom);
        }
    };

    const handleAreaChartZoomWithFocus = useCallback((): void => {
        d3.select('.areaChartZoomWithFocusSvg').select('svg').remove();

        const svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any> = d3.select('.areaChartZoomWithFocusSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white);

        d3.csv('https://gist.githubusercontent.com/mbostock/34f08d5e11952a80609169b7917d4172/raw/f553844d1d30582ec1f630fc0d692e4717c62329/sp500.csv').then((data) => {
            const parseData: AreaChartZoomWithFocusCsvType[] = [];

            data.forEach((item) => {
                parseData.push({
                    date: d3.timeParse('%b %Y')(item.date) as Date,
                    price: +item.price as number
                });
            });

            const x: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(d3.extent(parseData, (d: AreaChartZoomWithFocusCsvType): Date => d.date) as [Date, Date])
                .range([0, width]);

            const x2: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(x.domain())
                .range([0, width]);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(parseData, (d: AreaChartZoomWithFocusCsvType): number => d.price)] as [number, number])
                .range([height, 0]);

            const y2: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(y.domain())
                .range([height2, 0]);

            const focus: d3.Selection<SVGGElement, unknown, HTMLElement, any> = svg.append('g')
                .attr('class', 'focus')
                .attr('transform', `translate(${margin.left}, ${margin.top})`);

            const xAxis: d3.Selection<SVGGElement, unknown, HTMLElement, any> = focus.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            focus.append('g')
                .call(d3.axisLeft(y));

            svg.append('defs')
                .append('clipPath')
                .attr('id', 'clip')
                .append('rect')
                .attr('width', width)
                .attr('height', height);

            const area: d3.Selection<SVGPathElement, AreaChartZoomWithFocusCsvType[], HTMLElement, any> = focus.append('path')
                .datum(parseData)
                .attr('class', 'area')
                .attr('clip-path', 'url(#clip)')
                .attr('fill', colors.color_blue)
                .attr('d', d3.area<any>()
                    .x((d: AreaChartZoomWithFocusCsvType): number => x(d.date))
                    .y0(height)
                    .y1((d: AreaChartZoomWithFocusCsvType): number => y(+d.price))
                );

            const context: d3.Selection<SVGGElement, unknown, HTMLElement, any> = svg.append('g')
                .attr('class', 'context')
                .attr('transform', `translate(${margin2.left}, ${margin2.top})`);

            context.append('g')
                .attr('transform', `translate(0, ${height2})`)
                .call(d3.axisBottom(x2));

            context.append('path')
                .datum(parseData)
                .attr('fill', colors.color_blue)
                .attr('d', d3.area<any>()
                    .x((d: AreaChartZoomWithFocusCsvType): number => x2(d.date))
                    .y0(height2)
                    .y1((d: AreaChartZoomWithFocusCsvType): number => y2(d.price))
                );

            // 處理上半部面積圖放大動作效果
            const zoom = d3.zoom()
                .scaleExtent([1, 10])
                .translateExtent([[0, 0], [width, height]])
                .extent([[0, 0], [width, height]])
                .on('zoom', (event: any) => {
                    const t = event.transform;

                    x.domain(t.rescaleX(x2).domain());
                    xAxis.call(d3.axisBottom(x));

                    area.attr('d', d3.area<any>()
                        .x((d: AreaChartZoomWithFocusCsvType): number => x(d.date))
                        .y0(height)
                        .y1((d: AreaChartZoomWithFocusCsvType): number => y(d.price))
                    );

                    context.select('.brush').call(brush.move as any, x.range().map(t.invertX, t));
                });

            const brush = d3.brushX()
                .extent([[0, 0], [width, height2]])
                .on('brush end', (event) => {
                    const s = event.selection || x.range();

                    x.domain(s.map(x2.invert, x2));
                    xAxis.call(d3.axisBottom(x));

                    area.attr('d', d3.area<any>()
                        .x((d: AreaChartZoomWithFocusCsvType): number => x(d.date))
                        .y0(height)
                        .y1((d: AreaChartZoomWithFocusCsvType): number => y(d.price))
                    );
                });

            context.append('g')
                .attr('class', 'brush')
                .call(brush)
                .call(brush.move, x.range() as any);

            focus.call(zoom as any);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleAreaChartZoomWithFocus();
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [handleAreaChartZoomWithFocus]);

    return (
        <div className='areaChartZoomWithFocus'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='areaChartZoomWithFocusSvg' ref={areaChartZoomWithFocusRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AreaChartZoomWithFocus;