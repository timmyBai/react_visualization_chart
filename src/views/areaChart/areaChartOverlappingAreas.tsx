import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './areaChartOverlappingAreas.sass';

const AreaChartOverlappingAreas: React.FC = (): ReactNode => {
    const areaChartOverlappingAreasRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (areaChartOverlappingAreasRef.current?.clientWidth !== undefined) {
            setWidth(areaChartOverlappingAreasRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleAreaChartOverlappingAreas = useCallback((): void => {
        d3.select('.areaChartOverlappingAreasSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.areaChartOverlappingAreasSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);


        d3.csv('https://gist.githubusercontent.com/interwebjill/8122dd08da9facf8c6ef6676be7da03f/raw/2015371e8f74891c18d8498d4bfafdc8933c0a0f/kW_zoomed.csv').then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const parseData: AreaChartOverlappingAreasCsvType[] = [];

            data.forEach((item: d3.DSVRowString<string>) => {
                parseData.push({
                    date: d3.timeParse('%Y/%m/%d %H:%M')(item.date) as Date,
                    PVkW: +item.PVkW,
                    TBLkW: +item.TBLkW
                });
            });

            const x: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(d3.extent(parseData, (d: AreaChartOverlappingAreasCsvType) => d.date) as [Date, Date])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(parseData, (d: AreaChartOverlappingAreasCsvType) => d.PVkW)] as [number, number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.append('path')
                .datum(parseData)
                .attr('fill', colors.color_blue)
                .attr('opacity', 0.8)
                .attr('d', d3.area<any>()
                    .x((d: AreaChartOverlappingAreasCsvType) => x(d.date))
                    .y0(y(0))
                    .y1((d: AreaChartOverlappingAreasCsvType) => y(+d.PVkW))
                );

            svg.append('path')
                .datum(parseData)
                .attr('fill', colors.color_pink)
                .attr('opacity', 0.8)
                .attr('d', d3.area<any>()
                    .x(d => x(d.date))
                    .y0(y(0))
                    .y1((d: AreaChartOverlappingAreasCsvType) => y(+d.TBLkW))
                );
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleAreaChartOverlappingAreas();
        window.addEventListener('resize', () => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', () => {
                handleReSize();
            });
        };
    }, [handleAreaChartOverlappingAreas]);

    return (
        <div className='areaChartOverlappingAreas'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='areaChartOverlappingAreasSvg' ref={areaChartOverlappingAreasRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AreaChartOverlappingAreas;