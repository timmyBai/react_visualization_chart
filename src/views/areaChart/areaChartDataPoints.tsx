import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './areaChartDataPoints.sass';
import colors from '@/styles/colors.sass';

const AreaChartDataPoints: React.FC = (): ReactNode => {
    const areaChartPointsRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (areaChartPointsRef.current?.clientWidth !== undefined) {
            setWidth(areaChartPointsRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleAreaChartPoints = useCallback((): void => {
        d3.select('.areaChartPointsSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.areaChartPointsSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv').then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const parseData: AreaChartPointsCsvDataType[] = [];

            // parse data
            data.forEach((item: d3.DSVRowString<string>) => {
                parseData.push({
                    date: d3.timeParse('%Y-%m-%d')(item.date) as Date,
                    value: +item.value
                });
            });

            const data_ready: AreaChartPointsCsvDataType[] = parseData.slice(0, 90);

            const x: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(d3.extent(data_ready, (d: AreaChartPointsCsvDataType): Date => d.date) as [Date, Date])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(data_ready, (d: AreaChartPointsCsvDataType) => +d.value)] as [number, number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.append('path')
                .datum(data_ready)
                .attr('fill', colors.color_purple)
                .attr('d', d3.area<any>()
                    .x((d: AreaChartPointsCsvDataType): number => x(d.date) as number)
                    .y0(height)
                    .y1((d: AreaChartPointsCsvDataType): number => y(d.value))
                );

            svg.append('path')
                .datum(data_ready)
                .attr('fill', 'none')
                .attr('stroke', colors.color_dark_purple)
                .attr('stroke-width', 2)
                .attr('d', d3.line<any>()
                    .x((d: AreaChartPointsCsvDataType): number => x(d.date))
                    .y((d: AreaChartPointsCsvDataType): number => y(+d.value))
                );

            svg.selectAll('circle')
                .data(data_ready)
                .enter()
                .append('circle')
                .attr('r', 3)
                .attr('fill', colors.color_dark_purple)
                .attr('cx', (d: AreaChartPointsCsvDataType): number => x(d.date))
                .attr('cy', (d: AreaChartPointsCsvDataType): number => y(+d.value));
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleAreaChartPoints();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return (): void => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleAreaChartPoints]);

    return (
        <div className='areaChartPoints'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='areaChartPointsSvg' ref={areaChartPointsRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AreaChartDataPoints;