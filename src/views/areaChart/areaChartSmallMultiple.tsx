import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './areaChartSmallMultiple.sass';

const AreaChartSmallMultiple: React.FC = (): ReactNode => {
    const areaChartSmallMultipleRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (areaChartSmallMultipleRef.current?.clientWidth !== undefined) {
            setWidth(areaChartSmallMultipleRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 4 - margin.top - margin.bottom);
        }
    };

    const handleAreaChartSmallMultiple = useCallback((): void => {
        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/5_OneCatSevNumOrdered.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            d3.select('.areaChartSmallMultipleSvg').selectAll('.uniqueChart').remove();

            const sumstat: d3.InternMap<string, d3.DSVRowString<string>[]> = d3.group(data, (d) => d.name);

            const svg: d3.Selection<SVGGElement, [any, d3.DSVRowString<string>[]], d3.BaseType, unknown> = d3.selectAll('.areaChartSmallMultipleSvg')
                .selectAll('.uniqueChart')
                .data(sumstat)
                .enter()
                .append('div')
                .attr('class', 'uniqueChart w-full sm:w-6/12 md:w-6/12 lg:w-4/12 xl:w-4/12 xxl-w-4/12')
                .style('height', '25vh')
                .style('overflow', 'hidden')
                .append('svg')
                .attr('width', '100%')
                .attr('height', '25vh')
                .attr('viewbox', '-50 -50 100 100')
                .style('background-color', colors.color_white)
                .append('g')
                .attr('transform', `translate(${margin.left}, ${margin.top})`);

            const svgWidth: number = document.querySelector('svg')?.clientWidth || 0;
            const svgHeight: number = document.querySelector('svg')?.clientHeight || 0;

            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(d3.extent(data, (d: d3.DSVRowString<string>): number => +d.year) as [number, number])
                .range([0, svgWidth - margin.left - margin.right]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .call(d3.axisBottom(x).ticks(3));

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(data, (d: d3.DSVRowString<string>): number => +d.n)] as [number, number])
                .range([svgHeight - margin.top - margin.bottom, 0]);

            svg.append('g')
                .call(d3.axisLeft(y).ticks(5));

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .range(d3.schemeSet2);

            svg.append('path')
                .attr('fill', (d: any) => color(d[0]) as string)
                .attr('d', (d: [any, d3.DSVRowString<string>[]]): any => {
                    return d3.area<any>()
                        .x((d: AreaChartSmallMultipleCsvType): number => x(d.year))
                        .y0(y(0))
                        .y1((d: AreaChartSmallMultipleCsvType): number => y(d.n))(d[1]);
                });

            svg.append('text')
                .attr('x', 0)
                .attr('y', '-5')
                .attr('fill', d => color(d[0]) as string)
                .attr('text-anchor', 'start')
                .text((d: any) => d[0] as number);
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleAreaChartSmallMultiple();
        window.addEventListener('resize', () => {
            setTimeout(() => {
                handleReSize();
            }, 100);
        });

        return () => {
            window.removeEventListener('resize', () => {
                setTimeout(() => {
                    handleReSize();
                }, 100);
            });
        };
    }, [handleAreaChartSmallMultiple]);

    return (
        <div className='areaChartSmallMultiple'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='areaChartSmallMultipleSvg' ref={areaChartSmallMultipleRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AreaChartSmallMultiple;