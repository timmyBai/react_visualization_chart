import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './areaChartZoom.sass';

const AreaChartZoom: React.FC = (): ReactNode => {
    const areaChartZoomRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (areaChartZoomRef.current?.clientWidth !== undefined) {
            setWidth(areaChartZoomRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleAreaChartZoom = useCallback((): void => {
        d3.select('.areaChartZoomSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.areaChartZoomSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv').then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const parseData: AreaChartZoomCsvDataType[] = [];

            data.forEach((item) => {
                parseData.push({
                    date: d3.timeParse('%Y-%m-%d')(item.date) as Date,
                    value: +item.value
                });
            });

            const x: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(d3.extent(parseData, (d: AreaChartZoomCsvDataType): Date => d.date) as [Date, Date])
                .range([0, width]);

            const xAxis: d3.Selection<SVGGElement, unknown, HTMLElement, any> = svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(parseData, (d: AreaChartZoomCsvDataType): number => +d.value)] as [number, number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            // 新增切割器，切割面機圖邊緣
            svg.append('defs')
                .append('clipPath')
                .attr('id', 'clip')
                .append('rect')
                .attr('width', width)
                .attr('height', height)
                .attr('x', 0)
                .attr('y', 0);

            const line: d3.Selection<SVGPathElement, AreaChartZoomCsvDataType[], HTMLElement, any> = svg.append('path')
                .datum(parseData)
                .attr('clip-path', 'url(#clip)')
                .attr('fill', colors.color_purple)
                .attr('d', d3.area<any>()
                    .x((d: AreaChartZoomCsvDataType): number => x(d.date))
                    .y0(y(0))
                    .y1((d: AreaChartZoomCsvDataType): number => y(d.value))
                );

            const brushX = d3.brushX()
                .extent([[0, 0], [width, height]])
                .on('end', (e: any) => {
                    const extent = e.selection;

                    if (extent) {
                        x.domain([x.invert(extent[0]), x.invert(extent[1])]);

                        xAxis.transition().duration(1000).call(d3.axisBottom(x));

                        svg.select('.brush').call(brushX.move as any, null);

                        line.datum(parseData)
                            .transition()
                            .duration(1000)
                            .attr('d', d3.area<any>()
                                .x((d: AreaChartZoomCsvDataType): number => x(d.date))
                                .y0(y(0))
                                .y1((d: AreaChartZoomCsvDataType): number => y(d.value))
                            );
                    }
                });

            svg.append('g')
                .attr('class', 'brush')
                .call(brushX);

            svg.on('dblclick', (): void => {
                x.domain(d3.extent(parseData, (d: AreaChartZoomCsvDataType): Date => d.date) as [Date, Date]);

                xAxis.transition().call(d3.axisBottom(x));

                line.datum(parseData)
                    .transition()
                    .duration(1000)
                    .attr('d', d3.area<any>()
                        .x((d: AreaChartZoomCsvDataType): number => x(d.date))
                        .y0(y(0))
                        .y1((d: AreaChartZoomCsvDataType): number => y(d.value))
                    );
            });
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleAreaChartZoom();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return (): void => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleAreaChartZoom]);

    return (
        <div className='areaChartZoom'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='areaChartZoomSvg' ref={areaChartZoomRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AreaChartZoom;