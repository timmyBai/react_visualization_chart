import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './areaChartBasic.sass';

const AreaChartBasic: React.FC = (): ReactNode => {
    const areaChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (areaChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(areaChartBasicRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleAreaChartBasic = useCallback((): void => {
        d3.select('.arcChartBasicSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.arcChartBasicSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv').then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const parseData: AreaChartBasicCsvDataType[] = [];

            data.forEach((item: d3.DSVRowString<string>) => {
                parseData.push({
                    date: d3.timeParse('%Y-%m-%d')(item.date) as Date,
                    value: +item.value
                });
            });

            const x: d3.ScaleTime<number, number, never> = d3.scaleTime()
                .domain(d3.extent(parseData, (d: AreaChartBasicCsvDataType): Date => d.date) as [Date, Date])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(parseData, (d: AreaChartBasicCsvDataType): number => +d.value)] as [number, number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            svg.append('path')
                .datum(parseData)
                .attr('fill', colors.color_blue)
                .attr('d', d3.area<any>()
                    .x((d: AreaChartBasicCsvDataType): number => x(d.date))
                    .y0(y(0))
                    .y1((d: AreaChartBasicCsvDataType): number => y(d.value))
                );
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleAreaChartBasic();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleAreaChartBasic]);

    return (
        <div className='arcChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='arcChartBasicSvg' ref={areaChartBasicRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AreaChartBasic;