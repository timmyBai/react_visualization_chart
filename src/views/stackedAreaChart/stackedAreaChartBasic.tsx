import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './stackedAreaChartBasic.sass';

const StackedAreaChartBasic: React.FC = (): ReactNode => {
    const stackedAreaChartBasicRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (stackedAreaChartBasicRef.current?.clientWidth !== undefined) {
            setWidth(stackedAreaChartBasicRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleStackedAreaChartBasic = useCallback((): void => {
        d3.select('.stackedAreaChartBasicSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.stackedAreaChartBasicSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/5_OneCatSevNumOrdered.csv').then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(d3.extent(data, (d: d3.DSVRowString<string>): number => +d.year) as [number, number])
                .range([0, width]);

            svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, d3.max(data, (d: d3.DSVRowString<string>): number => +d.n) as number * 2] as [number, number])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const groups: d3.InternMap<number, d3.DSVRowString<string>[]> = d3.group(data, (d: d3.DSVRowString<string>): number => +d.year);

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .range([
                    colors.color_blue,
                    colors.color_light_blue,
                    colors.color_dark_purple,
                    colors.color_purple,
                    colors.color_pink,
                    colors.color_light_pink,
                    colors.color_black,
                    colors.color_light_black
                ]);

            const stackedKey: string[] = [];
            const matrix: number[] = [];

            groups.forEach((group: d3.DSVRowString<string>[]) => {
                group.forEach((item: d3.DSVRowString<string>) => {
                    if (!stackedKey.includes(item.name)) {
                        stackedKey.push(item.name);
                    }
                });
            });

            Array.from({ length: stackedKey.length }).forEach((item: unknown, index: number) => {
                matrix.push(index + 1);
            });

            const stackedData = d3.stack()
                .keys(matrix as any[])
                .value((d: any, key: string): number => {
                    return +d?.[1]?.[key]?.n ? +d?.[1]?.[key]?.n : 0;
                })(groups as any);

            svg.selectAll('mylayers')
                .data(stackedData)
                .enter()
                .append('path')
                .attr('fill', (d: d3.Series<{
                    [key: string]: number;
                }, string>): string => {
                    const name = stackedKey[d.index];

                    return color(name) as string;
                })
                .attr('d', d3.area<any>()
                    .x((d: any): number => x(d.data[0]))
                    .y0((d: any): number => y(d[0]))
                    .y1((d: any): number => y(d[1]))
                );
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleStackedAreaChartBasic();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return () => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleStackedAreaChartBasic]);

    return (
        <div className='stackedAreaChartBasic'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='stackedAreaChartBasicSvg' ref={stackedAreaChartBasicRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default StackedAreaChartBasic;