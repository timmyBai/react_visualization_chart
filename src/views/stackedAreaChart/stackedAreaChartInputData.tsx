import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './stackedAreaChartInputData.sass';

const StackedAreaChartInputData: React.FC = (): ReactNode => {
    const stackedAreaChartInputDataRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 20, right: 30, bottom: 30, left: 55 };

    const handleReSize = (): void => {
        if (stackedAreaChartInputDataRef.current?.clientWidth !== undefined) {
            setWidth(stackedAreaChartInputDataRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleStackedAreaChartInputData = useCallback((): void => {
        d3.select('.stackedAreaChartInputDataSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.stackedAreaChartInputDataSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/5_OneCatSevNumOrdered_wide.csv').then((data: d3.DSVRowArray<string>): Promise<void> | void => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(d3.extent(data, (d: d3.DSVRowString<string>): number => +d.year) as [number, number])
                .range([0, width]);

            svg.append('g')
                .attr('transform', `translate(0, ${height})`)
                .call(d3.axisBottom(x).ticks(5));

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 200000])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y));

            const keys: string[] = data.columns.slice(1);

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .domain(keys)
                .range([
                    colors.color_blue,
                    colors.color_light_blue,
                    colors.color_pink,
                    colors.color_light_pink,
                    colors.color_purple,
                    colors.color_dark_purple,
                    colors.color_light_black,
                    colors.color_black
                ]);

            const stackedData: d3.Series<{
                [key: string]: number;
            }, string>[] = d3.stack()
                .keys(keys)(data as any);

            svg.selectAll('myLayers')
                .data(stackedData)
                .enter()
                .append('path')
                .attr('fill', (d: any): string => color(d.key) as string)
                .attr('d', d3.area<any>()
                    .x((d: any): number => x(d.data.year))
                    .y0((d: any): number => y(d[0]))
                    .y1((d: any): number => y(d[1]))
                );
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleStackedAreaChartInputData();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return (): void => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleStackedAreaChartInputData]);

    return (
        <div className='stackedAreaChartInputData'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='stackedAreaChartInputDataSvg' ref={stackedAreaChartInputDataRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default StackedAreaChartInputData;