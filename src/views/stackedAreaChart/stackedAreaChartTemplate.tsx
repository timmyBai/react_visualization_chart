import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import colors from '@/styles/colors.sass';
import './stackedAreaChartTemplate.sass';

const StackedAreaChartTemplate: React.FC = (): ReactNode => {
    const stackedAreaChartTemplateRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin: MarginType = { top: 40, right: 40, bottom: 45, left: 55 };

    const handleReSize = (): void => {
        if (stackedAreaChartTemplateRef.current?.clientWidth !== undefined) {
            setWidth(stackedAreaChartTemplateRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const handleStackedAreaChartTemplate = useCallback((): void => {
        d3.select('.stackedAreaChartTemplateSvg').select('svg').remove();

        const svg: d3.Selection<SVGGElement, unknown, HTMLElement, any> = d3.select('.stackedAreaChartTemplateSvg')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .style('border-radius', '10px')
            .style('box-shadow', '0px 0px 15px rgba(0, 0, 0, 0.1)')
            .style('background-color', colors.color_white)
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        d3.csv('https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/5_OneCatSevNumOrdered_wide.csv').then((data: d3.DSVRowArray<string>): void | Promise<void> => {
            const x: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain(d3.extent(data, (d: d3.DSVRowString<string>): number => +d.year) as [number, number])
                .range([0, width]);

            const xAxis: d3.Selection<SVGGElement, unknown, HTMLElement, any> = svg.append('g')
                .call(d3.axisBottom(x))
                .attr('transform', `translate(0, ${height})`);

            const y: d3.ScaleLinear<number, number, never> = d3.scaleLinear()
                .domain([0, 200000])
                .range([height, 0]);

            svg.append('g')
                .call(d3.axisLeft(y).ticks(5));

            const keys: string[] = data.columns.slice(1);

            const stackedData: d3.Series<{
                [key: string]: number;
            }, string>[] = d3.stack()
                .keys(keys)(data as any);

            const color: d3.ScaleOrdinal<string, unknown, never> = d3.scaleOrdinal()
                .domain(keys)
                .range([
                    colors.color_blue,
                    colors.color_light_blue,
                    colors.color_pink,
                    colors.color_light_pink,
                    colors.color_purple,
                    colors.color_dark_purple,
                    colors.color_light_black,
                    colors.color_black
                ]);

            // path 切割器
            svg.append('defs')
                .append('clipPath')
                .attr('id', 'clip-path')
                .append('rect')
                .attr('width', width)
                .attr('height', height);

            const area: d3.Selection<SVGPathElement, d3.Series<{
                [key: string]: number;
            }, string>, SVGGElement, unknown> = svg.selectAll('myLayer')
                .data(stackedData)
                .enter()
                .append('path')
                .attr('clip-path', 'url(#clip-path)')
                .attr('fill', (d: any) => color(d.key) as string)
                .attr('d', d3.area<any>()
                    .x((d: any) => x(d.data.year))
                    .y0((d: any) => y(d[0]))
                    .y1((d: any) => y(d[1]))
                );

            const brushX: d3.BrushBehavior<unknown> = d3.brushX()
                .extent([[0, 0], [width, height]])
                .on('end', (e) => {
                    if (e.selection) {
                        const s: [number, number] = e.selection;

                        x.domain([x.invert(s[0]), x.invert(s[1])]).domain();

                        xAxis.transition().duration(1000).call(d3.axisBottom(x));

                        svg.select('.brush').call(brushX.move as any, null);

                        area.transition()
                            .duration(2000)
                            .attr('d', d3.area<any>()
                                .x((d: any) => x(d.data.year))
                                .y0((d: any) => y(d[0]))
                                .y1((d: any) => y(d[1]))
                            );
                    }
                });

            svg.on('dblclick', (): void => {
                x.domain(d3.extent(data, (d: d3.DSVRowString<string>): number => +d.year) as [number, number]).domain();

                xAxis.transition().duration(1000).call(d3.axisBottom(x));

                area.transition()
                    .duration(2000)
                    .attr('d', d3.area<any>()
                        .x((d: any): number => x(d.data.year))
                        .y0((d: any): number => y(d[0]))
                        .y1((d: any): number => y(d[1]))
                    );
            });

            svg.append('g')
                .call(brushX)
                .attr('class', 'brush');
        });
    }, [width, height]);

    useEffect(() => {
        handleReSize();
        handleStackedAreaChartTemplate();
        window.addEventListener('resize', (): void => {
            handleReSize();
        });

        return (): void => {
            window.removeEventListener('resize', (): void => {
                handleReSize();
            });
        };
    }, [handleStackedAreaChartTemplate]);

    return (
        <div className='stackedAreaChartTemplate'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl-w-12/12'>
                        <div className='stackedAreaChartTemplateSvg' ref={stackedAreaChartTemplateRef}>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default StackedAreaChartTemplate;