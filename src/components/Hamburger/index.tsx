import React, { ReactNode } from 'react';

// css
import './index.sass';

type HamburgerPropsType = {
    className?: string
    collapse?: () => void
};

const Hamburger: React.FC<HamburgerPropsType> = ({ className, collapse }): ReactNode => {
    return (
        <div className={`hamburger ${className}`} onClick={collapse}>
            <div className='hamburger_toggle'></div>
            <div className='hamburger_toggle'></div>
            <div className='hamburger_toggle'></div>
        </div>
    );
};

export default Hamburger;