import React, { ReactElement, ReactNode } from 'react';

// css
import './Menu.sass';

type MenuPropsType = {
    children: ReactElement | ReactElement[]
}

const Menu: React.FC<MenuPropsType> = ({ children }): ReactNode => {
    return (
        <ul className='menu w-56'>
            {children}
        </ul>
    );
};

export default Menu;
