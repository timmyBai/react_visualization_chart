import Menu from './Menu';
import MenuItem from './MenuItem';
import SubMenu from './SubMenu';

export default Object.assign(Menu, {
    Item: MenuItem,
    SubMenu: SubMenu
});