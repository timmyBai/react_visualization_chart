import React, { ReactElement, ReactNode } from 'react';

// css
import './MenuItem.sass';

type MenuItemPropsType = {
    children: ReactElement | ReactElement[]
}

const MenuItem: React.FC<MenuItemPropsType> = ({ children }): ReactNode => {
    return (
        <li className='menuItem'>
            {children}
        </li>
    );
};

export default MenuItem;
