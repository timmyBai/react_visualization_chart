import React, { ReactElement, ReactNode, useState } from 'react';

// css
import './SubMenu.sass';

type SubMenuPropsType = {
    title: string;
    children: ReactElement | ReactElement[]
};

const SubMenu: React.FC<SubMenuPropsType> = ({ title, children }): ReactNode => {
    const [isOpen, setIsOpen] = useState<boolean>(false);

    const handleSubMenuOpen = (): void => {
        setIsOpen(!!isOpen);
    };

    return (
        <li className='subMenu'>
            <details open={isOpen} onClick={handleSubMenuOpen}>
                <summary className='title'>{title}</summary>

                <ul className='nestMenu'>
                    {children}
                </ul>
            </details>
        </li>
    );
};

export default SubMenu;
