import Layout from '@/layout';
import LineChartBasic from '@/views/lineChart/lineChartBasic';
import LineChartGrouped from '@/views/lineChart/lineChartGrouped';
import LineChartSmallMultiple from '@/views/lineChart/lineChartSmallMultiple';
import LineChartConfidenceInterval from '@/views/lineChart/lineChartConfidenceInterval';
import LineChartColorGradient from '@/views/lineChart/lineChartColorGradient';
import LineChartInputDataTransfer from '@/views/lineChart/lineChartInputDataTransfer';
import LineChartGroupDropDown from '@/views/lineChart/lineChartGroupDropDown';
import LineChartFilterGroupDropDown from '@/views/lineChart/lineChartFilterGroupDropDown';
import LineChartZoom from '@/views/lineChart/lineChartZoom';
import LineChartCursorShowingExactValue from '@/views/lineChart/lineChartCursorShowingExactValue';

const lineChartRoutes = {
    path: '/lineChart',
    element: <Layout />,
    meta: {
        title: '折線圖'
    },
    alwaysShow: true,
    children: [
        {
            path: 'basic',
            element: <LineChartBasic />,
            meta: {
                title: '基礎折線圖'
            }
        },
        {
            path: 'grouped',
            element: <LineChartGrouped />,
            meta: {
                title: '多組折線圖'
            }
        },
        {
            path: 'smallMultiple',
            element: <LineChartSmallMultiple />,
            meta: {
                title: '小倍數折線圖'
            }
        },
        {
            path: 'confidenceInterval',
            element: <LineChartConfidenceInterval />,
            meta: {
                title: '信賴區間折線圖'
            }
        },
        {
            path: 'colorGradient',
            element: <LineChartColorGradient />,
            meta: {
                title: '漸層顏色折線圖'
            }
        },
        {
            path: 'interactivity',
            alwaysShow: true,
            meta: {
                title: '互動折現圖'
            },
            children: [
                {
                    path: 'inputDataTransfer',
                    element: <LineChartInputDataTransfer />,
                    meta: {
                        title: '輸入資料轉換折線圖'
                    }
                },
                {
                    path: 'groupDropDown',
                    element: <LineChartGroupDropDown />,
                    meta: {
                        title: '群組下拉選單折線圖'
                    }
                },
                {
                    path: 'filterGroupDropDown',
                    element: <LineChartFilterGroupDropDown />,
                    meta: {
                        title: '群組塞選下拉選單折線圖'
                    }
                },
                {
                    path: 'zoom',
                    element: <LineChartZoom />,
                    meta: {
                        title: '放大折線圖'
                    }
                },
                {
                    path: 'cursorShowingExactValue',
                    element: <LineChartCursorShowingExactValue />,
                    meta: {
                        title: '鼠標精確值折線圖'
                    }
                }
            ]
        }
    ]
};

export default lineChartRoutes;