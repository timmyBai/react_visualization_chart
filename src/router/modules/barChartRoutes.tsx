import Layout from '@/layout';
import BarChartBasic from '@/views/barChart/barChartBasic';
import BarChartHorizontal from '@/views/barChart/barChartHorizontal';
import BarChartOrdered from '@/views/barChart/barChartOrdered';
import BarChartAnimation from '@/views/barChart/barChartAnimation';
import BarChartChangeColor from '@/views/barChart/barChartChangeColor';
import BarChartChangeInputData from '@/views/barChart/barChartChangeInputData';
import BarChartChangeInputDataUpgrade from '@/views/barChart/barChartChangeInputDataUpgrade';
import BarChartChangeInputDataCsvVersion from '@/views/barChart/barChartChangeInputDataCsvVersion';
import BarChartStackedBasic from '@/views/barChart/barChartStackedBasic';
import BarChartGrouped from '@/views/barChart/barChartGrouped';
import BarChartPercentStacked from '@/views/barChart/barChartPercentStacked';
import BarChartStackedTooltip from '@/views/barChart/barChartStackedTooltip';
import BarChartGroupHighlightingStacked from '@/views/barChart/barChartGroupHighlightingStacked';

const barChartRoutes = {
    path: '/barChart',
    element: <Layout />,
    meta: {
        title: '長條圖'
    },
    alwaysShow: true,
    children: [
        {
            path: 'basic',
            element: <BarChartBasic />,
            meta: {
                title: '基礎長條圖'
            }
        },
        {
            path: 'horizontal',
            element: <BarChartHorizontal />,
            meta: {
                title: '水平長條圖'
            }
        },
        {
            path: 'ordered',
            element: <BarChartOrdered />,
            meta: {
                title: '有序長條圖'
            }
        },
        {
            path: 'interactivity',
            alwaysShow: true,
            meta: {
                title: '互動長條圖'
            },
            children: [
                {
                    path: 'animation',
                    element: <BarChartAnimation />,
                    meta: {
                        title: '動畫長條圖'
                    }
                },
                {
                    path: 'changeColor',
                    element: <BarChartChangeColor />,
                    meta: {
                        title: '變更顏色長條圖'
                    }
                },
                {
                    path: 'changeInputData',
                    element: <BarChartChangeInputData />,
                    meta: {
                        title: '變更資料輸入長條圖'
                    }
                },
                {
                    path: 'changeInputDataUpgrade',
                    element: <BarChartChangeInputDataUpgrade />,
                    meta: {
                        title: '變更資料輸入長條圖升級版'
                    }
                },
                {
                    path: 'changeInputDataCsvVersion',
                    element: <BarChartChangeInputDataCsvVersion />,
                    meta: {
                        title: '變更資料輸入長條圖 csv 版'
                    }
                },
                {
                    path: 'stackedBasic',
                    element: <BarChartStackedBasic />,
                    meta: {
                        title: '基礎堆疊長條圖'
                    }
                },
                {
                    path: 'grouped',
                    element: <BarChartGrouped />,
                    meta: {
                        title: '分組長條圖'
                    }
                },
                {
                    path: 'percentStacked',
                    element: <BarChartPercentStacked />,
                    meta: {
                        title: '百分比堆疊圖'
                    }
                },
                {
                    path: 'stackedTooltip',
                    element: <BarChartStackedTooltip />,
                    meta: {
                        title: '提示堆疊圖'
                    }
                },
                {
                    path: 'groupHighlightingStacked',
                    element: <BarChartGroupHighlightingStacked />,
                    meta: {
                        title: '群組突出堆疊長條圖'
                    }
                }
            ]
        }
    ]
};

export default barChartRoutes;