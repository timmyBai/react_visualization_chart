// components
import Layout from '@/layout/index';
import DensityBasic from '@/views/densityChart/densityChartBasic';
import DensityMultipleGroup from '@/views/densityChart/densityChartMultipleGroup';
import DensityChartMirror from '@/views/densityChart/densityChartMirror';
import DensityChartSidebar from '@/views/densityChart/densityChartSidebar';
import DensityChartDropdown from '@/views/densityChart/densityChartDropdown';
import DensityHistogramChart from '@/views/densityChart/densityHistogramChart';

const densityChartRoutes = {
    path: '/densityChart',
    element: <Layout />,
    meta: {
        title: '密度圖'
    },
    alwaysShow: true,
    children: [
        {
            path: 'basic',
            element: <DensityBasic />,
            meta: {
                title: '基礎密度圖'
            }
        },
        {
            path: 'multipleGroup',
            element: <DensityMultipleGroup />,
            meta: {
                title: '多組密度圖'
            }
        },
        {
            path: 'mirror',
            element: <DensityChartMirror />,
            meta: {
                title: '鏡向密度圖'
            }
        },
        {
            path: 'interactivity',
            alwaysShow: true,
            meta: {
                title: '互動密度圖'
            },
            children: [
                {
                    path: 'sidebar',
                    element: <DensityChartSidebar />,
                    meta: {
                        title: '滑塊密度圖'
                    }
                },
                {
                    path: 'dropdown',
                    element: <DensityChartDropdown />,
                    meta: {
                        title: '下拉選單密度圖'
                    }
                }
            ]
        },
        {
            path: 'combination',
            meta: {
                title: '組合圖'
            },
            alwaysShow: true,
            children: [
                {
                    path: 'histogram',
                    element: <DensityHistogramChart />,
                    meta: {
                        title: '密度直方組合圖'
                    }
                }
            ]
        }
    ]
};

export default densityChartRoutes;