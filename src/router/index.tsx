import React, { ReactNode } from 'react';
import { BrowserRouter, Navigate } from 'react-router-dom';
import RouterWaiter, { OnRouteBeforeType } from './components/RouterWaiter';

// routes
import densityChartRoutes from './modules/densityChartRoutes';
import barChartRoutes from './modules/barChartRoutes';
import lineChartRoutes from './modules/lineChartRoutes';

// utils
import { getPageTitle } from '@/utils/getPageTitle';

// components
import Layout from '@/layout';

import OrganizationChart from '@/views/organizationChart/index';
import HistogramChartBasic from '@/views/histogramChart/histogramChartBasic';
import HistogramChartToolTip from '@/views/histogramChart/histogramChartToolTip';
import HistogramChartColored from '@/views/histogramChart/histogramChartColored';
import HistogramChartDouble from '@/views/histogramChart/histogramChartDouble';
import HistogramChartCanChangeSizeBin from '@/views/histogramChart/histogramChartCanChangeSizeBin';

import CircularBarChartBasic from '@/views/circularBarChart/circularBarChartBasic';
import CircularBarChartLabel from '@/views/circularBarChart/circularBarChartLabel';
import CircularBarChartDouble from '@/views/circularBarChart/circularBarChartDouble';
import CircularBarChartStacked from '@/views/circularBarChart/circularBarChartStacked';
import CircularBarChartCaveat from '@/views/circularBarChart/circularBarChartCaveat';

import BoxPlotChartBasic from '@/views/boxPlotChart/boxPlotChartBasic';
import BoxPlotChartGroup from '@/views/boxPlotChart/boxPlotChartGroup';
import BoxPlotChartIndividualDataPoint from '@/views/boxPlotChart/boxPlotChartIndividualDataPoint';
import BoxPlotChartHorizontal from '@/views/boxPlotChart/boxPlotChartHorizontal';

import DonutChartBasic from '@/views/donutChart/donutChartBasic';
import DonutChartLabel from '@/views/donutChart/donutChartLabel';
import DonutChartChangeData from '@/views/donutChart/donutChartChangeData';
import DonutChartCleanLayout from '@/views/donutChart/donutChartCleanLayout';

import PieChartBasic from '@/views/pieChart/pieChartBasic';
import PieChartAnnotation from '@/views/pieChart/pieChartAnnotation';
import PieChartInputDataSelector from '@/views/pieChart/pieChartInputDataSelector';
import PieChartCensus from '@/views/pieChart/pieChartCensus';

import AreaChartBasic from '@/views/areaChart/areaChartBasic';
import AreaChartDataPoints from '@/views/areaChart/areaChartDataPoints';
import AreaChartSmallMultiple from '@/views/areaChart/areaChartSmallMultiple';
import AreaChartZoom from '@/views/areaChart/areaChartZoom';
import AreaChartAdvanced from '@/views/areaChart/areaChartAdvanced';
import AreaChartOverlappingAreas from '@/views/areaChart/areaChartOverlappingAreas';
import AreaChartZoomWithFocus from '@/views/areaChart/areaChartZoomWithFocus';

import StackedAreaChartBasic from '@/views/stackedAreaChart/stackedAreaChartBasic';
import StackedAreaChartInputData from '@/views/stackedAreaChart/stackedAreaChartInputData';

import HeatmapBasic from '@/views/heatmapChart/heatmapBasic';
import BubbleBasic from '@/views/bubbleChart/bubbleBasic';
import CorrelogramBasic from '@/views/correlogramChart/correlogramBasic';
import RidgelineChartBasic from '@/views/ridgelineChart/ridgelineChartBasic';
import ScatterChartBasic from '@/views/scatterChart/scatterChartBasic';
import SpiderChartBasic from '@/views/spiderChart/spiderChartBasic';
import ViolinChartBasic from '@/views/violinChart/violinChartBasic';

import ErrorPage404 from '@/views/errorPage/errorPage404';
import StackedAreaChartTemplate from '@/views/stackedAreaChart/stackedAreaChartTemplate';
import StreamChartBasic from '@/views/streamChart/streamChartBasic';
import StreamChartTemplate from '@/views/streamChart/streamChartTemplate';

type RoutersProps = {
    children: JSX.Element | JSX.Element[]
}

// base routes
export const constantRoutes = [
    {
        path: '',
        element: <Layout />,
        children: [
            {
                path: '',
                element: <OrganizationChart />,
                meta: {
                    title: '組織圖'
                }
            }
        ]
    },
    densityChartRoutes,
    barChartRoutes,
    {
        path: '/histogramChart',
        element: <Layout />,
        meta: {
            title: '直方圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <HistogramChartBasic />,
                meta: {
                    title: '基礎直方圖'
                }
            },
            {
                path: 'colored',
                element: <HistogramChartColored />,
                meta: {
                    title: '尾部色彩直方圖'
                }
            },
            {
                path: 'double',
                element: <HistogramChartDouble />,
                meta: {
                    title: '雙直方圖'
                }
            },
            {
                path: 'canChangeSizeBin',
                element: <HistogramChartCanChangeSizeBin />,
                meta: {
                    title: '可變大小直方圖'
                }
            },
            {
                path: 'interactivity',
                meta: {
                    title: '互動直方圖'
                },
                alwaysShow: true,
                children: [
                    {
                        path: 'tooltip',
                        element: <HistogramChartToolTip />,
                        meta: {
                            title: '提示直方圖'
                        }
                    }
                ]
            }
        ]
    },
    lineChartRoutes,
    {
        path: '/circularBarChart',
        element: <Layout />,
        alwaysShow: true,
        meta: {
            title: '圓形長條圖'
        },
        children: [
            {
                path: 'basic',
                element: <CircularBarChartBasic />,
                meta: {
                    title: '基礎圓形長條圖'
                }
            },
            {
                path: 'label',
                element: <CircularBarChartLabel />,
                meta: {
                    title: '標籤圓形長條圖'
                }
            },
            {
                path: 'double',
                element: <CircularBarChartDouble />,
                meta: {
                    title: '雙圓形長條圖'
                }
            },
            {
                path: 'stacked',
                element: <CircularBarChartStacked />,
                meta: {
                    title: '堆疊圓形長條圖'
                }
            },
            {
                path: 'caveat',
                element: <CircularBarChartCaveat />,
                meta: {
                    title: '警告圓形長條圖'
                }
            }
        ]
    },
    {
        path: '/boxPlotChart',
        element: <Layout />,
        meta: {
            title: '箱型圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <BoxPlotChartBasic />,
                meta: {
                    title: '基礎箱型圖'
                }
            },
            {
                path: 'group',
                element: <BoxPlotChartGroup />,
                meta: {
                    title: '群組箱型圖'
                }
            },
            {
                path: 'individualDataPoint',
                element: <BoxPlotChartIndividualDataPoint />,
                meta: {
                    title: '單獨資料點箱型圖'
                }
            },
            {
                path: 'horizontal',
                element: <BoxPlotChartHorizontal />,
                meta: {
                    title: '橫向箱型圖'
                }
            }
        ]
    },
    {
        path: '/donutChart',
        element: <Layout />,
        meta: {
            title: '圓環圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <DonutChartBasic />,
                meta: {
                    title: '基礎圓環圖'
                }
            },
            {
                path: 'label',
                element: <DonutChartLabel />,
                meta: {
                    title: '標籤圓環圖'
                }
            },
            {
                path: 'cleanLayout',
                element: <DonutChartCleanLayout />,
                meta: {
                    title: '清除佈局圓環圖'
                }
            },
            {
                path: 'changeData',
                element: <DonutChartChangeData />,
                meta: {
                    title: '變更資料圓環圖'
                }
            }
        ]
    },
    {
        path: '/pieChart',
        element: <Layout />,
        alwaysShow: true,
        meta: {
            title: '圓餅圖'
        },
        children: [
            {
                path: 'basic',
                element: <PieChartBasic />,
                meta: {
                    title: '基礎圓餅圖'
                }
            },
            {
                path: 'annotation',
                element: <PieChartAnnotation />,
                meta: {
                    title: '註解圓餅圖'
                }
            },
            {
                path: 'dataInputSelector',
                element: <PieChartInputDataSelector />,
                meta: {
                    title: '輸出資料選擇圓餅圖'
                }
            },
            {
                path: 'census',
                element: <PieChartCensus />,
                meta: {
                    title: '人口調查圓餅圖'
                }
            }
        ]
    },
    {
        path: '/areaChart',
        element: <Layout />,
        alwaysShow: true,
        meta: {
            title: '面積折線圖'
        },
        children: [
            {
                path: 'basic',
                element: <AreaChartBasic />,
                meta: {
                    title: '基礎面積折線圖'
                }
            },
            {
                path: 'dataPoints',
                element: <AreaChartDataPoints />,
                meta: {
                    title: '資料點面積折線圖'
                }
            },
            {
                path: 'smallMultiple',
                element: <AreaChartSmallMultiple />,
                meta: {
                    title: '小倍數面積折線圖'
                }
            },
            {
                path: 'zoom',
                element: <AreaChartZoom />,
                meta: {
                    title: '放大面積折線圖'
                }
            },
            {
                path: 'advanced',
                element: <AreaChartAdvanced />,
                meta: {
                    title: '進階面積折線圖'
                }
            },
            {
                path: 'overlappingAreas',
                element: <AreaChartOverlappingAreas />,
                meta: {
                    title: '重疊區域面積折線圖'
                }
            },
            {
                path: 'zoomWithFocus',
                element: <AreaChartZoomWithFocus />,
                meta: {
                    title: '縮放焦點面積折線圖'
                }
            }
        ]
    },
    {
        path: '/stackedAreaChart',
        element: <Layout />,
        meta: {
            title: '堆疊面積折線圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <StackedAreaChartBasic />,
                meta: {
                    title: '基礎堆疊面積折線圖'
                }
            },
            {
                path: 'inputData',
                element: <StackedAreaChartInputData />,
                meta: {
                    title: '輸入資料堆疊面積折線圖'
                }
            },
            {
                path: 'template',
                element: <StackedAreaChartTemplate />,
                meta: {
                    title: '模板面積折線圖'
                }
            }
        ]
    },
    {
        path: '/streamChart',
        element: <Layout />,
        meta: {
            title: '流程圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <StreamChartBasic />,
                meta: {
                    title: '基礎流程圖'
                }
            },
            {
                path: 'template',
                element: <StreamChartTemplate />,
                meta: {
                    title: '模版流程圖'
                }
            }
        ]
    },
    {
        path: '/bubbleChart',
        element: <Layout />,
        meta: {
            title: '氣泡圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <BubbleBasic />,
                meta: {
                    title: '基礎氣泡圖'
                }
            }
        ]
    },
    {
        path: '/correlogramChart',
        element: <Layout />,
        meta: {
            title: '相關圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <CorrelogramBasic />,
                meta: {
                    title: '基礎相關圖'
                }
            }
        ]
    },
    {
        path: '/heatmapChart',
        element: <Layout />,
        meta: {
            title: '熱點圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <HeatmapBasic />,
                meta: {
                    title: '基礎熱點圖'
                }
            }
        ]
    },
    {
        path: '/ridgelineChart',
        element: <Layout />,
        meta: {
            title: '山脊圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <RidgelineChartBasic />,
                meta: {
                    title: '基礎山脊圖'
                }
            }
        ]
    },
    {
        path: '/scatterChart',
        element: <Layout />,
        meta: {
            title: '散點圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <ScatterChartBasic />,
                meta: {
                    title: '基礎散點圖'
                }
            }
        ]
    },
    {
        path: '/spiderChart',
        element: <Layout />,
        meta: {
            title: '蜘蛛圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <SpiderChartBasic />,
                meta: {
                    title: '基礎蜘蛛圖'
                }
            }
        ]
    },
    {
        path: '/violinChart',
        element: <Layout />,
        meta: {
            title: '鋼琴圖'
        },
        alwaysShow: true,
        children: [
            {
                path: 'basic',
                element: <ViolinChartBasic />,
                meta: {
                    title: '基礎鋼琴圖'
                }
            }
        ]
    },
    {
        path: '/404',
        element: <ErrorPage404 />,
        meta: {
            title: '404'
        },
        hidden: true
    },
    {
        path: '*',
        element: <Navigate to='/404' />,
        hidden: true
    }
];

// permission routes
export const asyncRoutes = [
];

// routes component
const Router: React.FC<RoutersProps> = ({ children }): ReactNode => {
    const onRouteBefore: OnRouteBeforeType = ({ meta }) => {
        document.title = getPageTitle(meta.title);
    };

    return (
        <BrowserRouter>
            <RouterWaiter routes={constantRoutes} onRouteBefore={onRouteBefore}></RouterWaiter>
            {children}
        </BrowserRouter>
    );
};

export default Router;
