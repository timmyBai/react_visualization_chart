import { configureStore } from '@reduxjs/toolkit';
// import logger from 'redux-logger';
// import thunk from 'redux-thunk';

const modulesFiles = require.context('./modules', true, /\.ts/);

const modules = modulesFiles.keys().reduce((modules: any, modulePath: string) => {
    const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1');

    const value = modulesFiles(modulePath);

    modules[moduleName] = value.default[moduleName];

    return modules;
}, {});

const store = configureStore({
    reducer: {
        ...modules
    },
    middleware: getDefaultMiddleware => getDefaultMiddleware({
        serializableCheck: false
    })
    // .concat(logger, thunk)
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export default store;
