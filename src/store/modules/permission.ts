import { createSlice } from '@reduxjs/toolkit';
import { constantRoutes, asyncRoutes } from '@/router/index';

const initialState = {
    routes: constantRoutes
};

const reducers = {
    addRoutes: (state = initialState) => {
        state.routes = constantRoutes.concat(asyncRoutes);
    }
};

const permissionSlice = createSlice({
    name: 'permission',
    initialState,
    reducers
});

const permission = permissionSlice.reducer;

export default {
    permission
};
