import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState: AppStateType = {
    isLoading: false,
    sidebar: {
        opened: true,
        withoutAnimation: false
    },
    device: 'desktop'
};

const reducers = {
    changeLoading: (state = initialState, action: PayloadAction<boolean>) => {
        state.isLoading = action.payload;
    },
    toggleSideBar: (state = initialState) => {
        state.sidebar.opened = !state.sidebar.opened;
        state.sidebar.withoutAnimation = false;
    },
    toggleDevice: (state = initialState, action: PayloadAction<'desktop' | 'mobile'>) => {
        state.device = action.payload;
    },
    closeSideBar: (state = initialState, action: PayloadAction<{ withoutAnimation: boolean }>) => {
        state.sidebar.opened = false;
        state.sidebar.withoutAnimation = action.payload.withoutAnimation;
    }
};

const appSlice = createSlice({
    name: 'app',
    initialState,
    reducers
});

const app = appSlice.reducer;

export default {
    app
};