import { PayloadAction, createSlice } from '@reduxjs/toolkit';

const initialState = {
    catchView: [
        {
            path: '/organization/',
            title: '組織圖',
            showCloseIcon: false
        },
        {
            path: '/density/basic/',
            title: '基礎密度圖',
            showCloseIcon: false
        }
    ]
};

const reducers = {
    addTagsView: (state = initialState, action: PayloadAction<{title: string, path: string}>) => {
        let result = false;

        state.catchView.forEach((item) => {
            if (item.title === action.payload.title) {
                result = true;
            }
        });

        if (!result) {
            state.catchView.push({
                path: action.payload.path,
                title: action.payload.title,
                showCloseIcon: true
            });
        }
    },
    deleteTagsView: (state = initialState, action: PayloadAction<number>) => {
        const result = state.catchView.filter((item, index) => {
            if (index !== action.payload) {
                return item;
            }
        });

        state.catchView = result;
    }
};

const tagsViewSlice = createSlice({
    name: 'tagsView',
    initialState,
    reducers
});

const tagsView = tagsViewSlice.reducer;

export default {
    tagsView
};