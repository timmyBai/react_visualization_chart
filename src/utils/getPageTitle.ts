import defaultSettings from '@/settings';

const title = defaultSettings.title || 'react visualization chart';

export const getPageTitle = (pageTitle: string): string => {
    if (pageTitle) {
        return `${pageTitle} - ${title}`;
    }
    
    return `${title}`;
};